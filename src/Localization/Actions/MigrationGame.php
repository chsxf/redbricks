<?php

namespace App\Localization\Actions;

use Framework\Actions\RouterAwareAction;
use App\Blog\Table\GameTable;
use App\Blog\Table\GameContentTable;
use Framework\Renderer\RendererInterface;
use Framework\Router;
use Psr\Http\Message\ServerRequestInterface as Request;

class MigrationGame {

    protected $gameTable;
    protected $gameContentTable;

    use RouterAwareAction;

    public function __construct(
            RendererInterface $renderer, Router $router, GameTable $gameTable, GameContentTable $gameContentTable
    ) {
        $this->renderer = $renderer;
        $this->router = $router;
        $this->gameTable = $gameTable;
        $this->gameContentTable = $gameContentTable;
    }

    public function __invoke(Request $request) {
        $games = $this->gameTable->findAll();
        foreach ($games as $game) {
            $this->gameContentTable->insert([
                "game_id" => $game->getId(),
                "content" => $game->getContent(),
                "langcode" => 'en'
            ]);
        }
        //echo '<pre>';        var_dump($this->renderer); echo '</pre>'; die();

        return $this->redirect('blog.index');
    }

}


