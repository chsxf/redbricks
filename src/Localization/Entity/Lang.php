<?php

namespace App\Localization\Entity;

class Lang {

    /**
     *
     * @var int
     */
    protected $id;

    /**
     *
     * @var string
     */
    protected $code;

    /**
     *
     * @var string
     */
    protected $name;

    /**
     * 
     * @return int
     */
    function getId(): int {
        return $this->id;
    }

    /**
     * 
     * @return string
     */
    function getCode(): string {
        return $this->code;
    }

    /**
     * 
     * @return string
     */
    function getName(): string {
        return $this->name;
    }

    /**
     * 
     * @param int $id
     */
    function setId(int $id) {
        $this->id = $id;
    }

    /**
     * 
     * @param string $code
     */
    function setCode(string $code) {
        $this->code = $code;
    }

    /**
     * 
     * @param string $name
     */
    function setName(string $name) {
        $this->name = $name;
    }

}
