<?php

namespace App\Survey\Entity;

class Proposal {

    /**
     *
     * @var int
     */
    protected $id;

    /**
     *
     * @var int
     */
    protected $surveyId;

    /**
     *
     * @var string
     */
    protected $proposal;

    /**
     *
     * @var int
     */
    protected $image;

    /**
     * 
     * @return int
     */
    function getId(): int {
        return $this->id;
    }

    /**
     * 
     * @return int
     */
    function getSurveyId(): int {
        return $this->surveyId;
    }

    /**
     * 
     * @return string
     */
    function getProposal(): string {
        return $this->proposal;
    }

    /**
     * 
     * @param int $id
     */
    function setId(int $id) {
        $this->id = $id;
    }

    /**
     * 
     * @param int $surveyId
     */
    function setSurveyId(int $surveyId) {
        $this->surveyId = $surveyId;
    }

    /**
     * 
     * @param string $proposal
     */
    function setProposal(string $proposal) {
        $this->proposal = $proposal;
    }

    /**
     * 
     * @return int|null
     */
    function isImage(): ?int {
        return $this->image;
    }

    /**
     * 
     * @param int $image
     */
    function setImage(int $image) {
        $this->image = $image;
    }

}
