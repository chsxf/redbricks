<?php

namespace App\Survey\Entity;

class Survey {

    /**
     *
     * @var int
     */
    protected $id;

    /**
     *
     * @var int
     */
    protected $projectId;

    /**
     *
     * @var int
     */
    protected $userId;

    /**
     *
     * @var string
     */
    protected $question;

    /**
     *
     * @var \DateTime
     */
    protected $createdAt;

    /**
     *
     * @var \DateTime
     */
    protected $deadline;

    /**
     * 
     * @return int
     */
    function getId(): int {
        return $this->id;
    }

    /**
     * 
     * @return int
     */
    function getProjectId(): int {
        return $this->projectId;
    }

    /**
     * 
     * @return int
     */
    function getUserId(): int {
        return $this->userId;
    }

    /**
     * 
     * @return string|null
     */
    function getQuestion(): ?string {
        return $this->question;
    }

    /**
     * 
     * @return \DateTime
     */
    function getCreatedAt(): \DateTime {
        return $this->createdAt;
    }

    /**
     * 
     * @return \DateTime|null
     */
    function getDeadline(): ?\DateTime {
        return $this->deadline;
    }

    /**
     * 
     * @param int $id
     */
    function setId(int $id) {
        $this->id = $id;
    }

    /**
     * 
     * @param int $projectId
     */
    function setProjectId(int $projectId) {
        $this->projectId = $projectId;
    }

    /**
     * 
     * @param int $userId
     */
    function setUserId(int $userId) {
        $this->userId = $userId;
    }

    /**
     * 
     * @param string $question
     */
    function setQuestion(string $question) {
        $this->question = $question;
    }

    /**
     * 
     * @param \DateTime|string $datetime
     */
    function setCreatedAt($datetime) {
        if (is_string($datetime)) {
            $this->createdAt = new \DateTime($datetime);
        } else {
            $this->createdAt = $datetime;
        }
    }

    /**
     * 
     * @param \DateTime|string|null $datetime
     */
    function setDeadline($datetime) {
        if (empty($datetime)) {
            $this->deadline = null;
        }else if (is_string($datetime)) {
            $this->deadline = new \DateTime($datetime);
        } else {
            $this->deadline = $datetime;
        }
    }

}
