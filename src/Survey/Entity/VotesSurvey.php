<?php

namespace App\Survey\Entity;

class VotesSurvey {

    /**
     *
     * @var int
     */
    protected $id;

    /**
     *
     * @var int
     */
    protected $userId;

    /**
     *
     * @var int
     */
    protected $surveyId;

    /**
     *
     * @var int
     */
    protected $proposalId;

    /**
     * 
     * @return int
     */
    function getId(): int {
        return $this->id;
    }

    /**
     * 
     * @return int
     */
    function getUserId(): int {
        return $this->userId;
    }

    /**
     * 
     * @return int
     */
    function getSurveyId(): int {
        return $this->surveyId;
    }

    /**
     * 
     * @return int
     */
    function getProposalId(): int {
        return $this->proposalId;
    }

    /**
     * 
     * @param int $id
     */
    function setId(int $id) {
        $this->id = $id;
    }

    /**
     * 
     * @param int $userId
     */
    function setUserId(int $userId) {
        $this->userId = $userId;
    }

    /**
     * 
     * @param int $surveyId
     */
    function setSurveyId(int $surveyId) {
        $this->surveyId = $surveyId;
    }

    /**
     * 
     * @param int $proposalId
     */
    function setProposalId(int $proposalId) {
        $this->proposalId = $proposalId;
    }

}
