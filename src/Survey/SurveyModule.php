<?php

namespace App\Survey;

use Framework\Module;
use Framework\Router;
use Framework\Renderer\RendererInterface;
use Framework\Auth\LoggedInMiddleware;
use App\Survey\Actions\SurveyIndexAction;
use App\Survey\Actions\SurveyCrudAction;
use App\Survey\Actions\SurveyListAction;

class SurveyModule extends Module {

    public function __construct(Router $router, RendererInterface $renderer) {
        $renderer->addPath('survey', __DIR__ . "/views");
        $router->get('/surveys/index/{id:[0-9]+}', [LoggedInMiddleware::class, SurveyIndexAction::class], 'surveys');
        $router->crud("/surveys/{projectId:[0-9]+}", [LoggedInMiddleware::class, SurveyCrudAction::class], "surveys");
        $router->get("/home/{slug:[a-z\-0-9]+}-{id:[0-9]+}/surveys", SurveyListAction::class, "surveys.list");
        $router->post("/home/{slug:[a-z\-0-9]+}-{id:[0-9]+}/surveys", [LoggedInMiddleware::class, SurveyListAction::class]);
    }

}