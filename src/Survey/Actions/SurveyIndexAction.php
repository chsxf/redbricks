<?php

namespace App\Survey\Actions;

use Framework\Renderer\RendererInterface;
use App\Blog\Table\GameTable;
use App\Survey\Table\SurveyTable;
use App\Blog\Table\ContributorTable;
use Psr\Http\Message\ServerRequestInterface as Request;
use Framework\Session\SessionInterface;
use Framework\Auth;

class SurveyIndexAction {

    /**
     *
     * @var RendererInterface
     */
    protected $renderer;

    /**
     *
     * @var GameTable
     */
    protected $gameTable;

    /**
     *
     * @var SurveyTable
     */
    protected $surveyTable;

    /**
     *
     * @var ContributorTable
     */
    protected $contributorTable;

    /**
     *
     * @var SessionInterface
     */
    protected $session;

    public function __construct(
            RendererInterface $renderer, GameTable $gameTable, SurveyTable $surveyTable, ContributorTable $contributorTable, SessionInterface $session
    ) {
        $this->renderer = $renderer;
        $this->gameTable = $gameTable;
        $this->surveyTable = $surveyTable;
        $this->contributorTable = $contributorTable;
        $this->session = $session;
    }

    public function __invoke(Request $request) {
        $id = $request->getAttribute("id");
        $game = $this->gameTable->findShow($id);
        $this->validateAccessRights($game->getUserId(), $game->getId());
        $surveys = $this->surveyTable->findShow($id);
        return $this->renderer->render("@survey/index", compact("game", "surveys"));
    }

    protected function validateAccessRights(int $itemUserId, int $gameId) {
        $role = $this->session->get('auth.role');
        $userId = $this->session->get('auth.user');
        if ($role == null || ($role == 'user' && $userId != $itemUserId && !$this->contributorTable->isContributor($gameId, $userId))) {
            throw new Auth\ForbiddenException();
        }
    }

}
