<?php

namespace App\Blog\Entity;

class FeatureTag {

    /**
     *
     * @var int
     */
    protected $id;

    /**
     *
     * @var int
     */
    protected $featureId;

    /**
     *
     * @var int
     */
    protected $tagId;

    /**
     * 
     * @return int
     */
    function getId(): int {
        return $this->id;
    }

    /**
     * 
     * @return int
     */
    function getFeatureId(): int {
        return $this->featureId;
    }

    /**
     * 
     * @return int
     */
    function getTagId(): int {
        return $this->tagId;
    }

    /**
     * 
     * @param int $id
     */
    function setId(int $id) {
        $this->id = $id;
    }

    /**
     * 
     * @param int $featureId
     */
    function setFeatureId(int $featureId) {
        $this->featureId = $featureId;
    }

    /**
     * 
     * @param int $tagId
     */
    function setTagId(int $tagId) {
        $this->tagId = $tagId;
    }

}
