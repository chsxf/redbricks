<?php

namespace App\Blog;

use Framework\Upload;

class GameUpload extends Upload {

    protected $path;
    protected $formats = [
        "thumb" => [320, 180],
        "illustration" => [1024, 576]
    ];

    public function __construct(?int $projectId = 0) {
        $path = 'public/uploads/' . $projectId . '/game';
        parent::__construct($path);
    }

}
