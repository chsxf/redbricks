<?php

namespace App\Blog\Table;

use Framework\Database\Table;
use App\Blog\Entity\Contributor;
use Framework\Database\NoRecordException;

class ContributorTable extends Table {

    protected $entity = Contributor::class;
    protected $table = "contributor";

    public function isContributor(int $gameId, int $userId): bool {
        try {
            $this->makeQuery()
                    ->select('id')
                    ->where("c.game_id = $gameId AND c.user_id = $userId")
                    ->fetchOrFail();
            return true;
        } catch (NoRecordException $exception) {
            return false;
        }
    }

    public function findByGameId(int $gameId): array {
        return $this->findAll()
                ->select("c.user_id")
                ->where("c.game_id = $gameId")
                ->fetchColumn();
    }

}
