<?php

namespace App\Blog\Table;

use Framework\Database\Table;
use App\Blog\Entity\Tag;
use Framework\Database\Query;

class TagTable extends Table {

    protected $entity = Tag::class;
    protected $table = "tags";

    public function findAll(): Query {
        $name = 't.name_' . getenv("LANG");
        return $this->makeQuery()
                        ->select("t.id, $name as name, t.game_id, t.color");
    }

    public function findAllEn(): Query {
        return $this->makeQuery()
                        ->select("t.id, t.name_en as name, t.game_id, t.color");
    }
    
    public function findAllLang(): Query {
        return $this->makeQuery();
    }

    public function findShowEn(int $gameId) {
        return $this->findAllEn()
                ->where("t.game_id = $gameId");
    }
    
    public function findShow(int $game_id){
        return $this->findAll()
                ->where("t.game_id = $game_id");
    }

    public function findShowAllLang(int $game_id){
        return $this->findAllLang()
                ->where("t.game_id = $game_id");
    }

}
