<?php

namespace App\Blog\Table;

use App\Blog\Entity\LinkGamePlatform;
use Framework\Database\Table;

class LinkGamePlatformTable extends Table {

    protected $entity = LinkGamePlatform::class;
    public $table = "links_game_platform";

}
