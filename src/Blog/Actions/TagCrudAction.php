<?php

namespace App\Blog\Actions;

use Framework\Actions\CrudAction;
use Framework\Renderer\RendererInterface;
use Framework\Router;
use App\Blog\Table\TagTable;
use App\Blog\Table\GameTable;
use Framework\Session\FlashService;
use Psr\Http\Message\ServerRequestInterface;
use Framework\Session\SessionInterface;

class TagCrudAction extends CrudAction {

    /**
     *
     * @var RendererInterface
     */
    protected $renderer;

    /**
     *
     * @var Router
     */
    protected $router;

    /**
     *
     * @var TagTable
     */
    protected $table;

    /**
     *
     * @var GameTable
     */
    protected $gameTable;

    /**
     *
     * @var FlashService
     */
    protected $flash;

    /**
     *
     * @var SessionInterface
     */
    protected $session;
    protected $acceptedParams = ['name_en', 'name_es', 'name_fr', 'color'];

    public function __construct(
            RendererInterface $renderer, Router $router, TagTable $table, GameTable $gameTable, FlashService $flash, SessionInterface $session
    ) {
        parent::__construct($renderer, $router, $table, $flash);
        $this->gameTable = $gameTable;
        $this->session = $session;
    }

    public function edit(ServerRequestInterface $request) {
        $projectId = $request->getAttribute("projectId");
        $game = $this->gameTable->find($projectId);
        $this->validateAccessRights($game->getUserId(), $game->getId());
        if ($request->getMethod() === "POST") {
            $validator = $this->getValidator($request);
            if ($validator->isValid()) {
                $params = $request->getParsedBody();
                if ($params['name_fr'] == "" || $params['name_fr'] == "tag") {
                    $params['name_fr'] = $params['name_en'];
                }
                if ($params['name_es'] == "" || $params['name_es'] == "tag") {
                    $params['name_es'] = $params['name_en'];
                }
                $params = array_filter($params, function ($key) {
                    return in_array($key, $this->acceptedParams);
                }, ARRAY_FILTER_USE_KEY);
                //echo '<pre>'; var_dump($params, $request->getAttribute("id")); echo '</pre>'; die();
                $this->table->update($request->getAttribute("id"), $params);
                $this->flash->success(_("Tag updated"));
            } else {
                $this->flash->error(_("Error : check the fields"));
            }
        }
        // echo '<pre>'; var_dump($item, $projectId); echo '</pre>'; die();
        return $this->redirect('blog.user.features.tags', ['id' => $projectId]);
    }

    public function create(ServerRequestInterface $request) {
        $projectId = $request->getAttribute("projectId");
        $game = $this->gameTable->find($projectId);
        $this->validateAccessRights($game->getUserId(), $game->getId());
        $name = "tag";
        $this->table->insert([
            "name_en" => $name,
            "name_fr" => $name,
            "name_es" => $name,
            "game_id" => $projectId,
            "color" => 1
        ]);

        return $this->redirect('blog.user.features.tags', ['id' => $projectId]);
    }

    protected function validateAccessRights(int $gameUserId, int $gameId) {
        $role = $this->session->get('auth.role');
        $userId = $this->session->get('auth.user');
        if ($role == null ||
                ($role == 'user' && $userId != $gameUserId && !$this->contributorTable->isContributor($gameId, $userId))) {
            throw new Auth\ForbiddenException();
        }
    }

    protected function getValidator(ServerRequestInterface $request) {

        return parent::getValidator($request)
                        ->required("name_en", "color")
                        ->length("name_en", 2)
                        ->numeric("color");
    }

}
