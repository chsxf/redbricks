

<!--<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Test de Cron</title>
    </head>
    <body>
        ?php
         // Destinataire
        $to = "aline@ipld.fr";
        // Sujet
        $subject = 'Test de planification de tâche Cron';

        // Message
        $message = '
        <html>
          <head>
            <title>Test Cron</title>
          </head>
          <body>
            <table width="100%" border="0" cellspacing="0" cellpadding="5">
              <tr>
                <td align="center">
                  <p>
                    Ceci est un test qui prouve que Cron fonctionne correctement !
                  </p>
                  <p>
                    Chouette, hein ?
                  </p>
                </td>
              </tr>
            </table>
          </body>
        </html>
        ';

        // Pour envoyer un mail HTML, l en-tête Content-type doit être défini
        $headers = "MIME-Version: 1.0" . "\n";
        $headers .= "Content-type: text/html; charset=utf-8" . "\r\n";

        // En-têtes additionnels
        $headers .= 'From: Mail de test <no-reply@otterways.com>' . "\r\n";

        // Envoie
        $resultat = mail($to, $subject, $message, $headers);
        var_dump($resultat); die();
        ?>
    </body>
</html>-->

<?php
namespace App\Blog\Actions;

use App\Blog\Table\FeatureTable;
use App\Blog\Table\GameTable;
use App\Blog\Table\StatusTable;
use Framework\Response\RedirectResponse;
use Framework\Renderer\RendererInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use App\Notification\SendNotification;

class FeaturesCronAction {
    
    /**
     *
     * @var RendererInterface
     */
    protected $renderer;
    
    /**
     *
     * @var GameTable
     */
    protected $gameTable;
    
    /**
     *
     * @var StatusTable
     */
    protected $statusTable;
    
    /**
     *
     * @var FeatureTable
     */
    protected $featureTable;
    
    /**
     *
     * @var SendNotification
     */
    protected $sendNotification;

    public function __construct(
    RendererInterface $renderer, GameTable $gameTable, StatusTable $statusTable, FeatureTable $featureTable, SendNotification $sendNotification
    ) {
        $this->renderer = $renderer;
        $this->gameTable = $gameTable;
        $this->statusTable = $statusTable;
        $this->featureTable = $featureTable;
        $this->sendNotification = $sendNotification;
    }

    public function __invoke(Request $request) {
        //$features = $this->featureTable->findAll()->where("status_id = 3", "deadline >= CURDATE() AND deadline < CURDATE()+1")->fetchAll();
        $features = $this->featureTable->findAll()->where("id = 8")->fetchAll();
        foreach ($features as $feature) {
            //echo '<pre>'; var_dump($feature); echo '</pre>'; die();
            $this->sendNotification->send('deadline', [
                "gameId" => $feature->getProjectId(),
                "featureId" => $feature->getId()
            ]);
        }
        //var_dump($features); die();
        //return new RedirectResponse('/home');
    }

}
