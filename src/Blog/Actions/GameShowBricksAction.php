<?php

namespace App\Blog\Actions;

use Framework\Renderer\RendererInterface;
use Framework\Response\RedirectResponse;
use Psr\Http\Message\ServerRequestInterface;
use Framework\Router;
use App\Blog\Table\GameTable;
use App\Blog\Table\FeatureTable;
use App\Blog\Table\StatusTable;
use App\Shop\Table\PendingPurchaseTable;
use App\Shop\Table\PurchaseTable;
use App\Blog\Table\VotesTable;
use App\News\Table\NewsTable;
use App\Comments\Table\CommentsTable;
use App\Bug\Table\BugTable;
use App\Survey\Table\SurveyTable;
use App\Blog\Table\FeatureTagTable;
use App\Blog\Table\FeatureSummaryTable;
use Framework\Database\NoRecordException;

class GameShowBricksAction {

    /**
     * @var RendererInterface
     */
    private $renderer;

    /**
     *
     * @var FeatureTagTable
     */
    protected $featureTagTable;

    /**
     *
     * @var PurchaseTable
     */
    protected $purchaseTable;

    /**
     *
     * @var FeatureSummaryTable
     */
    protected $featureSummaryTable;

    public function __construct(
            RendererInterface $renderer, Router $router, GameTable $gameTable, FeatureTable $featureTable, StatusTable $statusTable, PendingPurchaseTable $pendingPurchaseTable, PurchaseTable $purchaseTable, VotesTable $votesTable, NewsTable $newsTable, CommentsTable $commentsTable, BugTable $bugTable, SurveyTable $surveyTable, FeatureTagTable $featureTagTable, FeatureSummaryTable $featureSummaryTable
    ) {
        $this->renderer = $renderer;
        $this->router = $router;
        $this->gameTable = $gameTable;
        $this->featureTable = $featureTable;
        $this->statusTable = $statusTable;
        $this->votesTable = $votesTable;
        $this->pendingPurchaseTable = $pendingPurchaseTable;
        $this->purchaseTable = $purchaseTable;
        $this->newsTable = $newsTable;
        $this->commentsTable = $commentsTable;
        $this->bugTable = $bugTable;
        $this->surveyTable = $surveyTable;
        $this->featureTagTable = $featureTagTable;
        $this->featureSummaryTable = $featureSummaryTable;
    }

    /**
     * @param ServerRequestInterface $request
     * @return RedirectResponse|string
     */
    public function __invoke(ServerRequestInterface $request) {
        $slug = $request->getAttribute("slug");
        $gameId = $request->getAttribute("id");
        $game = $this->gameTable->findShow($gameId);
        $hideArchiving = $request->getAttribute("hideArchiving") ? $request->getAttribute("hideArchiving") == "hide" : true;
        try {
            $summary = $this->featureSummaryTable->findShow($gameId);
        } catch (NoRecordException $e) {
            $summary = null;
        }

        $features = $this->featureTable->findShow($gameId);
        $status = $this->statusTable->findAll();
        $tags = $this->featureTagTable->findAll();
        $votes = $this->votesTable->getAllVote($gameId);
        $moneyRaised = $this->pendingPurchaseTable->getAllMoneyForProject($gameId);
        $moneyFunded = $this->purchaseTable->getAllMoneyRaisedForProject($gameId);
        foreach ($status->fetchColumn() as $numStatus) {
            $nbFeaturesSort[$numStatus] = $this->featureTable->findAll()->where("status_id = $numStatus", "project_id= $gameId")->count("*");
        }

        $nbFeatures = $this->featureTable->findShow($gameId)->where("s.id>1")->count("*");
        $nbBugs = $this->bugTable->findShow($gameId)->count("*");
        $nbComments = $this->commentsTable->findCount($gameId);
        $nbNews = $this->newsTable->findShow($gameId)->count("*");
        $nbSurveys = $this->surveyTable->findShow($gameId)->count("*");

        if ($game->getSlug() !== $slug) {
            return $this->redirect("blog.show", [
                        "slug" => $game->getSlug(),
                        "id" => $game->getId()
            ]);
        }

        return $this->renderer->render('@blog/listBricks', [
                    "game" => $game,
                    "features" => $features,
                    "status" => $status,
                    "nbFeaturesSort" => $nbFeaturesSort,
                    "votes" => $votes,
                    "moneyRaised" => $moneyRaised,
                    "moneyFunded" => $moneyFunded,
                    "nbFeatures" => $nbFeatures,
                    "nbBugs" => $nbBugs,
                    "nbComments" => $nbComments,
                    "nbNews" => $nbNews,
                    "nbSurveys" => $nbSurveys,
                    "tags" => $tags,
                    "hideArchiving" => $hideArchiving,
                    "summary" => $summary
        ]);
    }

}
