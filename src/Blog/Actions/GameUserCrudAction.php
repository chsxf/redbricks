<?php

namespace App\Blog\Actions;

use App\Auth\DatabaseAuth;
use App\Blog\Table\LinkGamePlatformTable;
use App\Blog\Table\FeatureTable;
use App\Shop\Table\PurchaseTable;
use App\Blog\Table\VotesTable;
use App\Blog\Table\LicenceTable;
use App\Blog\Table\PlatformTable;
use App\Blog\Table\GameTable;
use Framework\Renderer\RendererInterface;
use Framework\Router;
use Framework\Session\FlashService;
use Framework\Session\SessionInterface;
use Psr\Http\Message\ServerRequestInterface;
use App\Blog\Table\PrefixSocialMediaTable;
use App\Blog\Table\SocialMediaTable;
use App\Blog\Table\GameContentTable;
use App\Blog\Table\ContributorTable;
use App\Blog\Table\StatusGameTable;

class GameUserCrudAction extends GameCrudAction {

    protected $viewPath = "@blog" . DIRECTORY_SEPARATOR . "user" . DIRECTORY_SEPARATOR . "games";
    protected $routePrefix = "blog.user";

    /**
     * @var Auth
     */
    private $auth;

    public function __construct(
            RendererInterface $renderer, Router $router, GameTable $table, FlashService $flash, LicenceTable $licenceTable, PlatformTable $platformTable, FeatureTable $featureTable, VotesTable $votesTable, PurchaseTable $purchaseTable, LinkGamePlatformTable $linkGamePlatformTable, DatabaseAuth $auth, SessionInterface $session, \PDO $pdo, PrefixSocialMediaTable $prefixSocialMediaTable, SocialMediaTable $socialMediaTable, GameContentTable $gameContentTable, ContributorTable $contributorTable, StatusGameTable $statusGameTable
    ) {
        parent::__construct($renderer, $router, $table, $flash, $licenceTable, $platformTable, $linkGamePlatformTable, $featureTable, $votesTable, $purchaseTable, $session, $pdo, $prefixSocialMediaTable, $socialMediaTable, $gameContentTable, $contributorTable, $statusGameTable);
        $this->renderer = $renderer;
        $this->auth = $auth;
    }

    protected function index(ServerRequestInterface $request): string {
        $this->renderer->addGlobal("viewPath", $this->viewPath);
        $this->renderer->addGlobal("routePrefix", $this->routePrefix);
        $params = $request->getQueryParams();
        $user = $this->auth->getUser();
        //findPaginated(12): pagination réglée à 12
        //Si l'utilisateur est propriétaire de fonctionnalités, on recherche les jeux correspondants
        $features = $this->featureTable->findAllUser($user->getId())->distinct()->fetchAll();
        $stringFeatures = "";
        $j = count($features->records);
        for ($i = 0; $i < $j; $i++) {
            $stringFeatures .= $features->records[$i]['project_id'] . ",";
        }
        $stringFeatures = substr($stringFeatures, 0, -1);
        $gamesFeatures = $this->table->findIn("id", $stringFeatures);
        $gamesFeaturesIds = array_map(function($record) {
            return $record["id"];
        }, $gamesFeatures->records);
        $gamesFeaturesNames = array_map(function($record) {
            return $record["name"];
        }, $gamesFeatures->records);
        $gamesFeaturesUserId = array_map(function($record) {
            return $record["user_id"];
        }, $gamesFeatures->records);
        foreach ($gamesFeaturesUserId as $key => $value) {
            if ($value == $user->getId()) {
                unset($gamesFeaturesIds[$key]);
                unset($gamesFeaturesNames[$key]);
                unset($gamesFeaturesUserId[$key]);
            }
        }
        $items = $this->table->findAllUser($user->getId())->paginate(12, $params["p"] ?? 1);
        $urlStripe = "https://connect.stripe.com/oauth/authorize?response_type=code&client_id=ca_DFkD7GvrjxyE7AAKtfeMRQV3jJH5nLmk&scope=read_write";
        if (!empty($user->getStripeUserId())) {
            $urlStripe = "https://dashboard.stripe.com";
        } else if (!empty($user->getEmail())) {
            $urlStripe .= '&stripe_user[email]=' . $user->getEmail();
        }

        return $this->renderer->render($this->viewPath . "/index", compact("items", "gamesFeaturesIds", "gamesFeaturesNames", "gamesFeaturesUserId", "urlStripe"));
    }

}
