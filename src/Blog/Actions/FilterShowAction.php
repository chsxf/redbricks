<?php

namespace App\Blog\Actions;

use App\Blog\Table\LinkGamePlatformTable;
use App\Blog\Table\PlatformTable;
use App\Blog\Table\LicenceTable;
use App\Blog\Table\GameTable;
use App\Blog\Table\FeatureTable;
use Framework\Actions\RouterAwareAction;
use Framework\Renderer\RendererInterface;
use Psr\Http\Message\ServerRequestInterface as Request;

class FilterShowAction {

    /**
     * @var RendererInterface
     */
    private $renderer;

    /**
     * @var gameTable
     */
    private $gameTable;

    /**
     * @var featureTable
     */
    private $featureTable;

    /**
     * @var licenceTable
     */
    private $licenceTable;

    /**
     * @var platformTable
     */
    private $platformTable;

    /**
     * @var LinkGamePlatformTable
     */
    private $linkGamePlatformTable;

    protected $routePrefix = "blog.index";

    use RouterAwareAction;

    public function __construct(
            RendererInterface $renderer,
            GameTable $gameTable,
            FeatureTable $featureTable,
            LicenceTable $licenceTable,
            LinkGamePlatformTable $linkGamePlatformTable,
            PlatformTable $platformTable
    ) {
        $this->renderer = $renderer;
        $this->gameTable = $gameTable;
        $this->featureTable = $featureTable;
        $this->licenceTable = $licenceTable;
        $this->linkGamePlatformTable = $linkGamePlatformTable;
        $this->platformTable = $platformTable;
    }

    public function __invoke(Request $request) {
        //echo '<pre>';        var_dump($request); echo '</pre>'; die();
        $params = $request->getQueryParams();
        $lien = $request->getAttribute("search");

        $platforms = $this->platformTable->findIn("slug", $lien);
        $licences = $this->licenceTable->findIn("slug", $lien);

        $games = $this->gameTable->findPublic();
        if (!empty($platforms->records)) {
            $platformIds = array_map(function($record) {
                return $record["id"];
            }, $platforms->records);
            $games = $this->gameTable->filterPlatforms($games, $platformIds);
        }
        if (!empty($licences->records)) {
            $licenceIds = array_map(function($record) {
                return $record["id"];
            }, $licences->records);
            $games = $this->gameTable->filterLicences($games, $licenceIds);
        }
        //findPaginated(12): pagination réglée à 12
        $games = $games->paginate(12, $params["p"] ?? 1);

        $licences = $this->licenceTable->findAll();
        $platforms = $this->platformTable->findAll();
        $liens_platforms = $this->gameTable->findAllPlatform();
        $features = $this->featureTable->findAll();
        $this->renderer->addGlobal("routePrefix", $this->routePrefix);
        $inputSearch = "null";
        $page = $params["p"] ?? 1;
        //echo '<pre>';        var_dump(); echo '</pre>'; die();
        return $this->renderer->render("@blog/index", compact("games", "inputSearch", "liens_platforms", "lien", "filter", "licences", "licence", "platforms", "platform", "platforms_filter", "licences_filter", "slug", "page", "features"));
    }

}
