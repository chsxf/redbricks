<?php

namespace App\Blog\Actions;

use TreeHouse\Slugifier\Slugifier;
use Framework\Database\Hydrator;
use App\Blog\Entity\Game;
use App\Blog\GameUpload;
use App\Blog\Table\LinkGamePlatformTable;
use App\Blog\Table\LicenceTable;
use App\Blog\Table\PlatformTable;
use App\Blog\Table\GameTable;
use App\Blog\Table\FeatureTable;
use App\Blog\FeatureUpload;
use App\Shop\Table\PurchaseTable;
use App\Blog\Table\VotesTable;
use Framework\Auth;
use Framework\Actions\CrudAction;
use Framework\Renderer\RendererInterface;
use Framework\Router;
use Framework\Session\SessionInterface;
use Framework\Session\FlashService;
use Psr\Http\Message\ServerRequestInterface;
use App\Blog\Table\PrefixSocialMediaTable;
use App\Blog\Table\SocialMediaTable;
use App\Blog\Table\GameContentTable;
use App\Blog\Table\ContributorTable;
use App\Blog\Table\StatusGameTable;

class GameCrudAction extends CrudAction {

    protected $viewPath = "@blog/admin/games";
    protected $routePrefix = "blog.admin";
    protected $acceptedParams = ["name", "slug", "published", "fee", "licence", "graphics_licence", "music_licence", "status_id"];

    /**
     * @var licenceTable
     */
    private $licenceTable;

    /**
     * @var PlatformTable
     */
    private $platformTable;

    /**
     * @var LinkGamePlatformTable
     */
    private $linkGamePlatformTable;

    /**
     * @var PurchaseTable
     */
    private $purchaseTable;

    /**
     *
     * @var PrefixSocialMediaTable
     */
    private $prefixSocialMediaTable;

    /**
     *
     * @var SocialMediaTable
     */
    protected $socialMediaTable;

    /**
     *
     * @var GameContentTable
     */
    protected $gameContentTable;

    /**
     *
     * @var ContributorTable
     */
    protected $contributorTable;

    /**
     *
     * @var StatusGameTable
     */
    protected $statusGameTable;

    /**
     * @var \PDO
     */
    protected $pdo;

    public function __construct(
            RendererInterface $renderer, Router $router, GameTable $table, FlashService $flash, LicenceTable $licenceTable, PlatformTable $platformTable, LinkGamePlatformTable $linkGamePlatformTable, FeatureTable $featureTable, VotesTable $votesTable, PurchaseTable $purchaseTable, SessionInterface $session, \PDO $pdo, PrefixSocialMediaTable $prefixSocialMediaTable, SocialMediaTable $socialMediaTable, GameContentTable $gameContentTable, ContributorTable $contributorTable, StatusGameTable $statusGameTable
    ) {
        parent::__construct($renderer, $router, $table, $flash);
        $this->licenceTable = $licenceTable;
        $this->platformTable = $platformTable;
        $this->linkGamePlatformTable = $linkGamePlatformTable;
        $this->featureTable = $featureTable;
        $this->votesTable = $votesTable;
        $this->purchaseTable = $purchaseTable;
        $this->session = $session;
        $this->pdo = $pdo;
        $this->prefixSocialMediaTable = $prefixSocialMediaTable;
        $this->socialMediaTable = $socialMediaTable;
        $this->gameContentTable = $gameContentTable;
        $this->contributorTable = $contributorTable;
        $this->statusGameTable = $statusGameTable;
    }

    public function delete(ServerRequestInterface $request) {
        //S'il y a une transaction stripe, on ne peut pas supprimer ce jeu
        $purchase = $this->purchaseTable->findForProject($request->getAttribute('id'));
        //var_dump($purchase->records);die();
        if (!empty($purchase->records)) {
            $this->flash->error(_("Can't delete: There is a Stipe transaction for this project"));
            return $this->redirect($this->routePrefix . ".index");
        }
        //On supprime les images du jeu
        $game = $this->table->findEn($request->getAttribute('id'));
        $gameUpload = new GameUpload($game->getId());
        $featureUpload = new FeatureUpload($game->getId());
        $gameUpload->delete($game->getImage());
        //Les plateformes associées
        $this->delete1($request->getAttribute('id'), "platform");
        //Le jeu lui-même
        $this->table->delete($request->getAttribute("id"));
        //Les fonctionnalités avec leurs images et les votes associés
        $features = $this->featureTable->findShow($request->getAttribute('id'))->fetchAll();
        foreach ($features->records as $feature) {
            $votes = $this->votesTable->findBy("feature_id", $feature["id"])->fetchAll();
            foreach ($votes->records as $vote) {
                $this->votesTable->delete($vote["id"]);
            }
            $featureUpload->delete($feature->getImage());
            $this->featureTable->delete($feature->getId());
        }
        $this->flash->success(_("Successful deletion"));
        return $this->redirect($this->routePrefix . ".index");
    }

    protected function formParams(array $params): array {
        $params["licences"] = $this->licenceTable->findAll();
        $params["platforms"] = $this->platformTable->findAll();
        $params["prefixSocialMedia"] = $this->prefixSocialMediaTable->findAll();
        return $params;
    }

    protected function getNewEntity() {
        $game = new Game();
        $game->setCreatedAt(new \DateTime());
        $game->setFee(50);
        //echo '<pre>'; var_dump($game); echo '</pre>'; die();
        return $game;
    }

    protected function prePersist(ServerRequestInterface $request, $game): array {
        $params = array_merge($request->getParsedBody(), $request->getUploadedFiles());
        //$gameUpload = new GameUpload($game->getId());
        $image = $params['image']->getClientFilename(); //$gameUpload->upload($params["image"], $game->getImage());
        if ($image) {
            $params["image"] = $image;
            $this->acceptedParams[] = 'image';
        } else {
            unset($params["image"]);
        }

        if ($this->session->get('auth.role')) {
            $userId = $this->session->get('auth.user');
        } else {
            $userId = null;
        }
        if (isset($params["published"])) {
            $params["published"] = 1;
        }
        if (isset($params["fee"])) {
            $params["fee"] = $params["fee"] * 10;
        }

        $slugifier = new Slugifier();
        $params["slug"] = $slugifier->slugify($params["name"]);

        $params = array_filter($params, function ($key) {
            return in_array($key, $this->acceptedParams);
        }, ARRAY_FILTER_USE_KEY);
        //var_dump($params);die();
        return array_merge($params, ["updated_at" => date("Y-m-d H:i:s"),
            "created_at" => $game->getCreatedAt()->format("Y-m-d H:i:s"),
            "user_id" => $userId]);
    }

    protected function getValidator(ServerRequestInterface $request) {
        //var_dump($request);die();
        $validator = parent::getValidator($request)
                ->required("name", "content")
                ->length("name", 2, 50)
                ->length("content", 2)
                ->extension("image", ["jpg", "png", "gif", "bmp"])
                ->size("image", 1024 * 1024); //1Mo
        //Si on veut rendre l'image obligatoire en création
        //En création, $request->getAttribute("id") est null
        if (is_null($request->getAttribute("id"))) {
            $validator->uploaded("image");
        }
        return $validator;
    }

    protected function validateAccessRights(?int $itemUserId, ?int $gameId) {
        $role = $this->session->get('auth.role');
        $userId = $this->session->get('auth.user');
        if ($role == null || ($role == 'user' && $userId != $itemUserId && !$this->contributorTable->isContributor($gameId, $userId))) {
            throw new Auth\ForbiddenException();
        }
    }

    /**
     * Action de suppression d'une plateforme
     *
     */
    public function delete1(int $game_id, string $file) {
        $name = "linkGame" . ucfirst($file) . "Table";
        $query = $this->pdo->prepare("DELETE FROM {$this->$name->table} WHERE game_id=?");
        $query->execute([$game_id]);
        return;
    }

    /**
     * Action de suppression d'une plateforme
     *
     */
    public function delete2(int $game_id, string $link_id, string $file) {
        $name = "linkGame" . ucfirst($file) . "Table";
        $field = $file . "_id";
        $query = $this->pdo->prepare("DELETE FROM {$this->$name->table} WHERE $field IN ($link_id) AND (game_id= ?) ");
        //var_dump($query,$link_id, $game_id);die();
        $query->execute([$game_id]);
        return;
    }

    /**
     * Crée un nouvel élément
     * @param Request $request
     * @return ResponseInterface|string
     */
    public function create(ServerRequestInterface $request) {
        $item = $this->getNewEntity();
        $statusList = $this->statusGameTable->findAll();

        if ($request->getMethod() === "POST") {
            $validator = $this->getValidator($request);
            $params = array_merge($request->getParsedBody(), $request->getUploadedFiles());
            //echo '<pre>'; var_dump($params, $item); echo '</pre>'; die();
            if ($validator->isValid()) {
                $this->table->insert($this->prePersist($request, $item));
                $this->postPersist($request, $item);
                //On récupère l'id de l'insertion
                //et on créée tous les liens pour ce jeu
                $id = $this->table->getPdo()->lastInsertId();
                $this->gameContentTable->insert([
                    "game_id" => $id,
                    "content" => $params['content'],
                    "langcode" => 'en'
                ]);
                $this->create1($id, $params["platform"], "platform");
                $this->updateMedia($id, $params["media"]);
                $gameUpload = new GameUpload($id);
                $gameUpload->upload($params["image"]);
                $this->flash->success($this->messages["create"]);
                return $this->redirect($this->routePrefix . ".index");
            }
            if (isset($params['fee'])) {
                $params['fee'] = $params['fee'] * 10;
            }
            if (isset($params['image'])) {
                $params['image'] = $params['image']->getClientFilename();
            }
            Hydrator::hydrate($params, $item);
            $medias = Hydrator::hydrateMedia($params['media']);
            if (isset($params['platform'])) {
                $linkPlatforms = Hydrator::hydratePlatform($params['platform']);
            }
            $errors = $validator->getErrors();
        }

        return $this->renderer->render(
                        $this->viewPath . "/create", $this->formParams(compact("item", "errors", "medias", "linkPlatforms", "statusList"))
        );
    }

    protected function updateMedia(int $game_id, array $params) {
        $queryInsert = $this->pdo->prepare("INSERT into {$this->socialMediaTable->getTable()} "
                . "(project_id, media_id, address) VALUES (:project_id, :media_id, :address)");
        $queryDelete = $this->pdo->prepare("DELETE FROM {$this->socialMediaTable->getTable()} "
                . "WHERE project_id=:project_id AND media_id=:media_id");
        foreach ($params as $index => $address) {
            $queryDelete->bindValue(':project_id', $game_id);
            $queryDelete->bindValue(':media_id', $index);
            $queryDelete->execute();
            if ($address != null) {
                $queryInsert->bindValue(':project_id', $game_id);
                $queryInsert->bindValue(':media_id', $index);
                $queryInsert->bindValue(':address', $address);
                $queryInsert->execute();
            }
        }
        $queryInsert->closeCursor();
        $queryDelete->closeCursor();
    }

    /**
     * Crée enregistrements dans linkGamePlatformTable
     */
    public function create1(int $game_id, array $params, string $file) {
        $fields = "game_id," . $file . "_id";
        $values = ":game_id, :" . $file . "_id";
        $name = "linkGame" . ucfirst($file) . "Table";
        $query = $this->pdo->prepare("INSERT into {$this->$name->table} ($fields) VALUES ($values)");
        foreach ($params as $param) {
            $query->bindValue(':game_id', $game_id);
            $query->bindValue(':' . $file . '_id', $param);
            $query->execute();
        }
        $query->closeCursor();

        return;
    }

    /**
     * Edite un jeu
     * @param Request $request
     * @return ResponseInterface|string
     */
    public function edit(ServerRequestInterface $request) {
        $id = (int) $request->getAttribute('id');
        $item = $this->table->findEn($id);
        $this->validateAccessRights($item->getUserId(), $item->getId());
        $platforms = $this->linkGamePlatformTable->findAll()->where("game_id = :field")->params(["field" => $id])->fetchAll();
        $platformIds = array_map(function($record) {
            return $record["platform_id"];
        }, $platforms->records);
        $medias = $this->socialMediaTable->findByGame($id);
        $params = array_merge($request->getParsedBody(), $request->getUploadedFiles());
        $linkPlatforms = Hydrator::hydratePlatform($platformIds);
        $statusList = $this->statusGameTable->findAll();

        if ($request->getMethod() === "POST") {
            $validator = $this->getValidator($request);
            //var_dump($request);die();
            if ($validator->isValid()) {
                $fields = $this->prePersist($request, $item);
                $this->updateMedia($id, $params["media"]);

                //on modifie l'image
                $gameUpload = new GameUpload($id);
                $gameUpload->upload($params['image'], $item->getImage());

                //On enlève le user_id qui est le dernier élément
                array_pop($fields);
                $this->table->update($id, $fields);
                $this->gameContentTable->updateByGame($id, ["content" => $params['content']]);
                if (!isset($params["platform"])) {
                    $params["platform"] = array();
                }
                //var_dump($params);die();
                //On compare les plateformes éditées avec celles de la requête
                $platformAdds = (array_diff($params["platform"], $platformIds));
                $platformDels = (array_diff($platformIds, $params["platform"]));
                $platformDels = join(",", $platformDels);
                //On crée les nouvelles platformes
                if (!empty($platformAdds)) {
                    $this->create1($id, $platformAdds, "platform");
                }
                //Et on supprime les platformes non cochées
                if (!empty($platformDels)) {
                    $this->delete2($id, $platformDels, "platform");
                }

                $this->postPersist($request, $item);
                $this->flash->success($this->messages["edit"]);
                return $this->redirect($this->routePrefix . ".index");
            }
            $errors = $validator->getErrors();
            $medias = Hydrator::hydrateMedia($params['media']);
            $linkPlatforms = Hydrator::hydratePlatform($params['platform']);
            if (isset($params['fee'])) {
                $params['fee'] = $params['fee'] * 10;
            }
            Hydrator::hydrate($params, $item);
        }
        return $this->renderer->render(
                        $this->viewPath . "/edit", $this->formParams(compact("item", "errors", "medias", "linkPlatforms", "statusList"))
        );
    }

}
