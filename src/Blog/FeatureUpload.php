<?php

namespace App\Blog;

use Framework\Upload;

class FeatureUpload extends Upload {

    protected $path;
    protected $formats = [
        "thumb" => [320, 180],
        "illustration" => [1024, 576]
    ];

    public function __construct(?int $projectId = 0) {
        $path = 'public/uploads/' . $projectId . '/features';
        parent::__construct($path);
    }

}
