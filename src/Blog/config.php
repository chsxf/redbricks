<?php

use App\Blog\BlogModule;
use function \Di\object;
use function \Di\get;

return [
    "blog.prefix" => "/home",
    "admin.widgets" => \DI\add([
        get(\App\Blog\BlogWidget::class)
    ]),
    "user.widgets" => \DI\add([
        get(\App\Blog\UserBlogWidget::class)
    ])
];
