<?php

namespace App\Support\Table;

use Framework\Database\Table;
use App\Support\Entity\Section;

class SectionTable extends Table {
    
    protected $entity = Section::class;
    protected $table = "support_sections";
}
