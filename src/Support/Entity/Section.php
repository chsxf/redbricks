<?php

namespace App\Support\Entity;

class Section {

    /**
     *
     * @var int
     */
    protected $id;

    /**
     *
     * @var string
     */
    protected $nameFr;

    /**
     *
     * @var string
     */
    protected $slugFr;

    /**
     * 
     * @return int
     */
    function getId(): int {
        return $this->id;
    }

    /**
     * 
     * @return string
     */
    function getNameFr(): string {
        return $this->nameFr;
    }

    /**
     * 
     * @return string
     */
    function getSlugFr(): string {
        return $this->slugFr;
    }

    /**
     * 
     * @param int $id
     */
    function setId(int $id) {
        $this->id = $id;
    }

    /**
     * 
     * @param string $nameFr
     */
    function setNameFr(string $nameFr) {
        $this->nameFr = $nameFr;
    }

    /**
     * 
     * @param string $slugFr
     */
    function setSlugFr(string $slugFr) {
        $this->slugFr = $slugFr;
    }

}
