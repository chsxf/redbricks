<?php

namespace App\Bug\Entity;

use Framework\Entity\Timestamp;

class Bug {

    /**
     *
     * @var int
     */
    protected $id;

    /**
     *
     * @var string
     */
    protected $title;

    /**
     *
     * @var int
     */
    protected $projectId;

    /**
     *
     * @var int
     */
    protected $statusId;

    /**
     *
     * @var string
     */
    protected $statusName;

    /**
     *
     * @var string
     */
    protected $description;

    /**
     *
     * @var int
     */
    protected $userId;

    /**
     *
     * @var string
     */
    protected $author;

    use Timestamp;

    /**
     * 
     * @param int $id
     */
    public function setId(int $id) {
        $this->id = $id;
    }

    /**
     * 
     * @return int
     */
    public function getId(): int {
        return $this->id;
    }

    /**
     * 
     * @return string|null
     */
    public function getTitle(): ?string {
        return $this->title;
    }

    /**
     * 
     * @return int
     */
    public function getProjectId(): int {
        return $this->projectId;
    }

    /**
     * 
     * @return int|null
     */
    public function getStatusId(): ?int {
        return $this->statusId;
    }

    /**
     * 
     * @return string|null
     */
    public function getStatusName(): ?string {
        return $this->statusName;
    }

    /**
     * 
     * @return string|null
     */
    public function getDescription(): ?string {
        return $this->description;
    }

    /**
     * 
     * @return int
     */
    public function getUserId(): int {
        return $this->userId;
    }

    /**
     * 
     * @return string
     */
    public function getAuthor(): string {
        return $this->author;
    }

    /**
     * 
     * @param string $title
     */
    public function setTitle(string $title) {
        $this->title = $title;
    }

    /**
     * 
     * @param int $projectId
     */
    public function setProjectId(int $projectId) {
        $this->projectId = $projectId;
    }

    /**
     * 
     * @param int $statusId
     */
    public function setStatusId(int $statusId) {
        $this->statusId = $statusId;
    }

    /**
     * 
     * @param string $statusName
     */
    public function setStatusName(string $statusName) {
        $this->statusName = $statusName;
    }

    /**
     * 
     * @param string $description
     */
    public function setDescription(string $description) {
        $this->description = $description;
    }

    /**
     * 
     * @param int $userId
     */
    public function setUserId(int $userId) {
        $this->userId = $userId;
    }

    /**
     * 
     * @param string $author
     */
    public function setAuthor(string $author) {
        $this->author = $author;
    }

}
