<?php

namespace App\Discussion\Table;

use Framework\Database\Table;
use App\Discussion\Entity\ThreadUnread;
use Framework\Database\QueryResult;

class ThreadUnreadTable extends Table {

    protected $table = "thread_unread";
    protected $entity = ThreadUnread::class;

    /**
     * 
     * @param int $userId
     * @return QueryResult
     */
    public function findbyUser(int $userId): QueryResult {
        return $this->findAll()
                        ->where("t.user_id = $userId")
                        ->fetchAll();
    }

    /**
     * 
     * @param int $threadId
     * @param int $userId
     * @return bool
     */
    public function deleteByThread(int $threadId, int $userId): bool {
        $query = $this->pdo->prepare("DELETE FROM {$this->table} WHERE thread_id=? AND user_id=?");
        return $query->execute([$threadId, $userId]);
    }

    /**
     * 
     * @param array $params
     * @return bool
     */
    public function insertUnique(array $params): bool {
        $fields = array_keys($params);
        $values = join(",", array_map(function ($field) {
                    return ":" . $field;
                }, $fields));
        $fields = join(",", $fields);
        $query = $this->pdo->prepare("INSERT IGNORE into {$this->table} ($fields) VALUES ($values)");
        return $query->execute($params);
    }

}
