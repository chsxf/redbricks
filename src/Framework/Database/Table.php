<?php

namespace Framework\Database;

class Table {

    /**
     * @var \PDO
     */
    protected $pdo;

    /**
     * Nom de la table en BDD
     * @var string
     */
    protected $table;

    /**
     * Entité à utiliser
     * @var string | null
     */
    protected $entity = \stdClass::class;

    public function __construct(\PDO $pdo) {
        $this->pdo = $pdo;
    }

    /**
     * Récupère une liste clef valeur de nos enregistrements
     */
    public function findList(): array {
        $results = $this->pdo
                ->query("SELECT id, name FROM {$this->table}")
                ->fetchAll(\PDO::FETCH_NUM);
        $list = [];
        foreach ($results as $result) {
            $list[$result[0]] = $result[1];
        }
        return $list;
    }

    /**
     * @return Query
     */
    public function makeQuery(): Query {
        //$this->table[0] est l'alias (1ère lettre du nom de la table)
        return (new Query($this->pdo))
                        ->from($this->table, $this->table[0])
                        ->into($this->entity);
    }

    /**
     * Récupère tous les enregistrements
     * @return Query
     */
    public function findAll(): Query {

        return $this->makeQuery();
    }

    /**
     * Récupère tous les enregistrements par rapport à un champ
     * @return Query
     */
    public function findAllBy(string $field, string $value): Query {

        return $this->makeQuery()
                ->where("$field = $value");
    }
    
    /**
     * Récupère une ligne par rapport à un champ
     */
    public function findBy(string $field, string $value) {
        return $this->makeQuery()->where("$field = :field")->params(["field" => $value])->fetchOrFail();
    }

    /**
     * Récupère une ligne par rapport à un champ dans un array
     */
    public function findIn(string $field, string $value) {
        $string = explode(",", $value);
        $values = join(",", array_map(function ($value) {
                    return "'" . $value . "'";
                }, $string));

        //var_dump($this->makeQuery()->where("$field IN ( $values )"));die();
        return $this->makeQuery()->where("$field IN ( $values )")->fetchAll();
    }

    /**
     * Récupère un élément à partir de son id
     * @param mixed
     * @return mixed
     */
    public function find(int $id) {
        return $this->makeQuery()->where("id = $id")->fetchOrFail();
    }

    /**
     * Récupère le nombre d'enregistrements
     */
    public function count(): int {
        return $this->makeQuery()->count();
    }

    public function sum(string $field): ?int {
        return $this->makeQuery()->sum($field);
    }

    /**
     * Récupère le nombre d'enregistrements filtré par rapport à un champ
     */
    public function findByAndCount(string $field, string $value): int {
        return $this->makeQuery()->where("$field = :field")->params(["field" => $value])->count();
    }

    /**
     * Met un jour un enregistrement au niveau de la base de données
     * @param int $id
     * @param array $params
     * @return bool
     */
    public function update(int $id, array $params): bool {
        $fieldQuery = $this->buildFieldQuery($params);
        //echo '<pre>'; var_dump($fieldQuery, $params); echo '</pre>'; die();
        $params["id"] = $id;
        $query = $this->pdo->prepare("UPDATE {$this->table} SET $fieldQuery WHERE id= :id");
        return $query->execute($params);
    }

    /**
     * Crée un enregistrement au niveau de la base de données
     * @param array $params
     * @return bool
     */
    public function insert(array $params): bool {
        $fields = array_keys($params);
        $values = join(",", array_map(function ($field) {
                    return ":" . $field;
                }, $fields));
        $fields = join(",", $fields);
        $query = $this->pdo->prepare("INSERT into {$this->table} ($fields) VALUES ($values)");
        return $query->execute($params);
    }
    
    /**
     * 
     * @param array $params
     * @return Query
     */
    public function insertUnique(array $params): bool {
        $fields = array_keys($params);
        $values = join(",", array_map(function ($field) {
                    return ":" . $field;
                }, $fields));
        $fields = join(",", $fields);
        $query = $this->pdo->prepare("INSERT IGNORE into {$this->table} ($fields) VALUES ($values)");
        return $query->execute($params);
    }

    /**
     * Supprime un enregistrement au niveau de la base de données
     * @param int $id
     * @return bool
     */
    public function delete(int $id): bool {
        $query = $this->pdo->prepare("DELETE FROM {$this->table} WHERE id=?");
        return $query->execute([$id]);
    }

    public function deleteBy(string $field, string $value) {
        $query = $this->pdo->prepare("DELETE FROM {$this->table} WHERE $field = :value");
        return $query->execute(["value" => $value]);
    }

    protected function buildFieldQuery(array $params) {
        return join(", ", array_map(function ($field) {
                    return "$field= :$field";
                }, array_keys($params)));
    }

    /**
     * @return mixed
     */
    public function getEntity(): string {
        return $this->entity;
    }

    /**
     * @return mixed
     */
    public function getTable(): string {
        return $this->table;
    }

    /**
     * @return mixed
     */
    public function getRequest(): string {
        return $this->request_tables;
    }

    /**
     * Vérifie qu'un enregistrement existe
     * @param $id
     * @return bool
     */
    public function exists($id): bool {
        $query = $this->pdo->prepare("SELECT id FROM {$this->table} WHERE id=?");
        $query->execute([$id]);
        return $query->fetchColumn() !== false;
    }

    /**
     * @return \PDO
     */
    public function getPdo(): \PDO {
        return $this->pdo;
    }

}
