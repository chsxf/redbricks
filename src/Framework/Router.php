<?php

namespace Framework;

use Framework\Router\Route;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Expressive\Router\FastRouteRouter;
use Zend\Expressive\Router\Route as ZendRoute;

/**
 * Class Router
 * Recense et Représente les routes suivies
 */
class Router {

    /**
     * @var FastRouteRouter
     */
    private $router;

    public function __construct(?string $cache = null) {
        $this->router = new FastRouteRouter(null, null);
    }

    /**
     * @param string $path
     * @param string/callable $callable
     * @param string $name
     */
    public function get(string $path, $callable, ?string $name = null) {

        $this->router->addRoute(new ZendRoute($path, $callable, ["GET"], $name));
    }

    /**
     * @param string $path
     * @param string/callable $callable
     * @param string $name
     */
    public function delete(string $path, $callable, ?string $name = null) {
        $this->router->addRoute(new ZendRoute($path, $callable, ["DELETE"], $name));
    }

    /**
     * @param string $path
     * @param $callable
     * @param null|string $name
     */
    public function any(string $path, $callable, ?string $name = null) {

        $this->router->addRoute(new ZendRoute($path, $callable, ['DELETE', 'POST', 'GET', 'PUT'], $name));
    }

    /**
     * Génère les routes du CRUD
     * @param string $prefixPath
     * @param callable $callable
     * @param string $prefixName
     */
    public function crud(string $prefixPath, $callable, string $prefixName) {
        $this->get("$prefixPath", $callable, "$prefixName.index");
        //var_dump($prefixPath, $callable,$prefixName, $params);
        $this->get("$prefixPath/new", $callable, "$prefixName.create");
        $this->post("$prefixPath/new", $callable);
        $this->get("$prefixPath/{id:\d+}", $callable, "$prefixName.edit");
        $this->post("$prefixPath/{id:\d+}", $callable);
        $this->delete("$prefixPath/{id:\d+}", $callable, "$prefixName.delete");
        //$this->get("$prefixPath/{id:\d+, slug}", $callable, "$prefixName.index");
    }

    /**
     * @param string $path
     * @param string/callable $callable
     * @param string $name
     */
    public function post(string $path, $callable, ?string $name = null) {
        $this->router->addRoute(new ZendRoute($path, $callable, ["POST"], $name));
    }

    /**
     * @param ServerRequestInterface $request
     * @return Route/null
     * ?Route signifie:
     * "Type declarations for parameters and return values
     * can now be marked as nullable by prefixing the type name with
     * a question mark. This signifies that as well as the specified type,
     * NULL can be passed as an argument, or returned as a value,
     * respectively."
     */
    public function match(ServerRequestInterface $request): ?Route {
        $result = $this->router->match($request);
        if ($result->isSuccess()) {
            return new Route(
                    $result->getMatchedRouteName(),
                    $result->getMatchedMiddleWare(),
                    $result->getMatchedParams()
            );
        }
        return null;
    }

    public function generateUri(string $name, array $params = [], array $queryParams = []): ?string {
        $uri = $this->router->generateUri($name, $params);
        if (!empty($queryParams)) {
            return $uri . "?" . http_build_query($queryParams);
        }
        return $uri;
    }

}
