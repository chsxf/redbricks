<?php
namespace Framework\Session;

interface SessionInterface
{
    
    /**
    * Récupère une information de session
    * @param $string $key
    * @param mixed $default
    * @return mixed
    */
    public function get(string $key, $default = null);

        
    /**
    * Ajoute une information de session
    * @param $string $key
    * @param $value
    * @return mixed
    */
    public function set(string $key, $default): void ;
    
    /**
    * Supprime une clef en session
    * @param $string $key
    */
    public function delete(string $key): void;
}
