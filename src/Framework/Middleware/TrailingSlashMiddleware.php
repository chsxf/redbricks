<?php
namespace Framework\Middleware;

use Psr\Http\Message\ServerRequestInterface;

class TrailingSlashMiddleware
{

    /**
    * Vérifie si on a un / à la fin URL
    */
    public function __invoke(ServerRequestInterface $request, callable $next)
    {
        $uri = $request->getUri()->getPath();
        if (!empty($uri) && $uri[-1] === '/' && $uri !== '/') {
            return (new \GuzzleHttp\Psr7\Response())
                ->withStatus(301)
                ->withHeader('Location', substr($uri, 0, -1));
        }

        return $next($request);
    }
}
