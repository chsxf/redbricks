<?php

namespace Framework\Middleware;

use Framework\Auth;
use Psr\Http\Message\ServerRequestInterface;
use Interop\Http\ServerMiddleware\DelegateInterface;
use Psr\Http\Message\ResponseInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use App\Auth\User;

class LocalizationMiddleware implements MiddlewareInterface {

    /**
     * @var Auth
     */
    private $auth;

    public function __construct(Auth $auth) {
        $this->auth = $auth;
    }

    public function process(ServerRequestInterface $request, DelegateInterface $delegate): ResponseInterface {
        $user = $this->auth->getUser();
        if ($user !== null) {
            //$user->setLangInterface();
            $langcode = $user->getLangcode();
        } else if (isset($_COOKIE['langcode'])) {
            $langcode = $_COOKIE['langcode'];
            if ($langcode != 'en' && $langcode != 'fr' && $langcode != 'es') {
                $langcode = 'en';
                setcookie("langcode", '', time() - 3600, '/', 'redbricks.games', true, true);
            }
        } else {
            $langcode = 'en';
        }
        User::setLang($langcode);
        setlocale(LC_NUMERIC, 'C');

        return $delegate->process($request);
    }

}
