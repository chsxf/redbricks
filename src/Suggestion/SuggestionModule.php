<?php

namespace App\Suggestion;

use Framework\Module;
use Framework\Router;
use Framework\Renderer\RendererInterface;
use App\Suggestion\Actions\SuggestionAction;
use Framework\Auth\LoggedInMiddleware;

class SuggestionModule extends Module {

    public function __construct(Router $router, RendererInterface $renderer) {
        $renderer->addPath('suggestion', __DIR__ . '/views');
        $router->get('/suggest-project', SuggestionAction::class, 'suggestion');
        $router->get('/suggest-project/{category:studio|game|votes}-{sort:up|down}', SuggestionAction::class, 'suggestion.sort');
        $router->post('/suggest-project', [LoggedInMiddleware::class, SuggestionAction::class]);
    }

}
