<?php
namespace App\Auth;

use Framework\Auth\ForbiddenException;
use Framework\Response\RedirectResponse;
use Framework\Router;
use Framework\Session\FlashService;
use Framework\Session\SessionInterface;
use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class ForbiddenMiddleware implements MiddlewareInterface
{

    /**
     * @var string
     */
    private $loginPath;
    /**
     * @var SessionInterface
     */
    private $session;
	/**
     * @var RouterInterface
     */
    private $router;

    public function __construct(string $loginPath, SessionInterface $session, Router $router)
    {
        $this->loginPath = $loginPath;
        $this->session = $session;
		$this->router = $router;
    }

    /**
     * @param ServerRequestInterface $request
     * @param DelegateInterface $delegate
     * @return ResponseInterface
     * @throws \TypeError
     */
    public function process(ServerRequestInterface $request, DelegateInterface $delegate): ResponseInterface
    {
        //var_dump($delegate);die();
		try {
            return $delegate->process($request);
        } catch (ForbiddenException $exception) {
			$role= $this->session->get('auth.role');
			if ($role != null) {
				return $this->redirectRole($request, $role);
			} else {
				return $this->redirectLogin($request);
			}
        } catch (\TypeError $error) {
            if (strpos($error->getMessage(), \Framework\Auth\User::class) !== false) {
                return $this->redirectLogin($request);
            }
            throw $error;
        }
    }

    public function redirectLogin(ServerRequestInterface $request): ResponseInterface
    {
        $this->session->set('auth.redirect', $request->getUri()->getPath());
        (new FlashService($this->session))->error(_('You must log in to access this page.'));
        return new RedirectResponse($this->loginPath);
    }
	
	public function redirectRole(ServerRequestInterface $request, string $role): ResponseInterface
	{
		(new FlashService($this->session))->error(_("You can't access this page."));
		$path = $this->router->generateUri($role);
		return new RedirectResponse($path);
	}
}
