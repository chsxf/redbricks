<?php

namespace App\Auth;

use Framework\Database\Table;
use Ramsey\Uuid\Uuid;
use App\Shop\Table\PendingPurchaseTable;
use App\Shop\Table\PurchaseTable;
use App\Shop\Table\SubscriptionTable;

class UserTable extends Table {

    protected $table = "users";

    public function __construct(\PDO $pdo, string $entity = User::class) {
        $this->entity = $entity;
        parent::__construct($pdo);
    }

    /**
     * 
     * @param int $id
     * @return string
     */
    public function resetPassword(int $id): string {
        $token = Uuid::uuid4()->toString();
        $this->update($id, [
            'password_reset' => $token,
            'password_reset_at' => date('Y-m-d H:i:s')
        ]);
        return $token;
    }

    /**
     * 
     * @param int $id
     * @param string $password
     * @return void
     */
    public function updatePassword(int $id, string $password): void {
        $this->update($id, [
            'password' => password_hash($password, PASSWORD_DEFAULT),
            'password_reset' => null,
            'password_reset_at' => null
        ]);
    }

    /**
     * 
     * @param int $id
     */
    public function activateAccount(int $id) {
        $this->update($id, ['active' => 1]);
    }
    
    public function contributorsForProject(int $gameId) {
        $purchase = new PurchaseTable($this->pdo);
        $pendingPurchase = new PendingPurchaseTable($this->pdo);
        $subscription = new SubscriptionTable($this->pdo);
        return $this->makeQuery()
                ->select("u.*")
                ->join($purchase->getTable() . " as pu", "pu.user_id = u.id")
                ->join($pendingPurchase->getTable() . " as pe", "pe.user_id = u.id")
                ->join($subscription->getTable() . " as s", "s.user_id = u.id")
                ->where("pu.game_id = $gameId OR pe.game_id = $gameId OR s.game_id = $gameId")
                ->order("s.created_at, pe.created_at, pu.created_at")
                ->groupBy("u.username");
    }
}
