<?php

namespace App\Shop\Table;

use Framework\Database\Table;
use App\Shop\Entity\PendingPurchase;
use App\Shop\Table\StripeUserTable;
use App\Blog\Table\GameTable;
use App\Auth\UserTable;
use Framework\Database\Query;
use App\Auth\User;
use Framework\Database\QueryResult;

class PendingPurchaseTable extends Table {

    protected $entity = PendingPurchase::class;
    protected $table = "pending_purchases";

    /**
     * 
     * @param string $field
     * @param string $value
     * @return mixed
     */
    public function findLastBy(string $field, string $value) {
        $stripeUserTable = new StripeUserTable($this->pdo);
        $gameTable = new GameTable($this->pdo);
        $userTable = new UserTable($this->pdo);
        return $this->makeQuery()
                        ->select("p.*, s.customer_id as customerId, g.name as projectName, u.stripe_user_id as stripeUserId")
                        ->join($stripeUserTable->getTable() . " as s", "s.user_id = p.user_id")
                        ->join($gameTable->getTable() . " as g", "g.id = p.game_id")
                        ->join($userTable->getTable() . " as u", "u.id = g.user_id")
                        ->where("$field = :field")
                        ->params(["field" => $value])
                        ->order("p.created_at DESC")
                        ->limit(1)
                        ->fetch();
    }

    /**
     * 
     * @param int $featureId
     * @return bool
     */
    public function deleteAllByFeature(int $featureId): bool {
        $query = $this->pdo->prepare("DELETE FROM {$this->table} WHERE feature_id =?");
        return $query->execute([$featureId]);
    }

    /**
     * 
     * @param int $game_id
     * @return Query
     */
    public function getAllMoneyForProject(int $game_id): Query {
        return $this->makeQuery()
                        ->select('SUM(price)/100 as price, feature_id')
                        ->where("game_id = $game_id")
                        ->groupBy('feature_id');
    }

    /**
     * 
     * @param string $field
     * @param int $id
     * @return mixed
     */
    public function getMoneyRaised(string $field, int $id) {
        return $this->makeQuery()
                        ->select('SUM(price)/100')
                        ->where("$field = $id")
                        ->fetchColumnNum();
    }

    /**
     * 
     * @return Query
     */
    public function getAllMoneyRaised(): Query {
        return $this->makeQuery()
                        ->select('SUM(price)/100 as price, p.game_id')
                        ->groupBy('game_id');
    }

    /**
     * 
     * @param string $field
     * @param int $id
     * @return mixed
     */
    public function countParticipants(string $field, int $id) {
        return $this->makeQuery()
                        ->select("count(DISTINCT user_id)")
                        ->where("$field = $id")
                        ->fetchColumnNum();
    }

    /**
     * 
     * @param User $user
     * @return QueryResult
     */
    public function findForUser(User $user): QueryResult {
        return $this->makeQuery()
                        ->select('p.*')
                        ->where('p.user_id = :user')
                        ->params(['user' => $user->getId()])
                        ->order("p.created_at DESC")
                        ->fetchAll();
    }

}
