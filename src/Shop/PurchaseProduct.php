<?php

namespace App\Shop;

use App\Auth\User;
use App\Blog\Table\GameTable;
use App\Blog\Table\FeatureTable;
use App\Shop\Table\PurchaseTable;
use App\Shop\Table\PendingPurchaseTable;
use App\Shop\Table\StripeUserTable;
use App\Shop\Table\SubscriptionTable;
use App\Auth\UserTable;
use Framework\Api\Stripe;
use Stripe\Card;
use Stripe\Customer;
use Stripe\Plan;

class PurchaseProduct {

    /**
     * @var PurchaseTable
     */
    protected $purchaseTable;

    /**
     * @var PendingPurchaseTable
     */
    protected $pendingPurchaseTable;

    /**
     * @var Stripe
     */
    protected $stripe;

    /**
     * @var StripeUserTable
     */
    protected $stripeUserTable;

    /**
     * @var UserTable
     */
    protected $userTable;

    /**
     *
     * @var SubscriptionTable
     */
    protected $subscriptionTable;

    public function __construct(
            PurchaseTable $purchaseTable, PendingPurchaseTable $pendingPurchaseTable, GameTable $gameTable, FeatureTable $featureTable, UserTable $userTable, Stripe $stripe, StripeUserTable $stripeUserTable, SubscriptionTable $subscriptionTable
    ) {
        $this->purchaseTable = $purchaseTable;
        $this->pendingPurchaseTable = $pendingPurchaseTable;
        $this->gameTable = $gameTable;
        $this->featureTable = $featureTable;
        $this->userTable = $userTable;
        $this->stripe = $stripe;
        $this->stripeUserTable = $stripeUserTable;
        $this->subscriptionTable = $subscriptionTable;
    }

    /**
     * Génère l'achat du produit pour l'utilisateur en utilisant Stripe
     * @param User $user
     * @param string $token
     * @throws AlreadyPurchasedException
     */
    public function process(int $amount, User $user, string $token, int $gameId, int $featureId, float $feeUser) {
        // Récupérer les informations de la contribution
        $game = $this->gameTable->find($gameId);
        $this->gameTable->update($gameId, [
            "updated_at" => date("Y-m-d H:i:s")
        ]);
        $stripeUserId = $this->userTable->find($game->getUserId())->getStripeUserId();
        $nameGame = $game->getName();
        if ($featureId > 0) {
            $feature = $this->featureTable->find($featureId);
            $name = $feature->getName();
        } else {
            $name = $nameGame;
            $featureId = null;
        }
        $feeTot = $game->getFee();

        // Calculer le prix TTC
        $card = $this->stripe->getCardFromToken($token);
        // Calculer les taxes dessus
        $price = ($amount - $feeUser) * 100;
        $fee = $price * $feeTot / 100;
        $gift = $feeUser * 100;

        // Créer ou récupérer le customer de l'utilisateur
        $customer = $this->findCustomerForUser($user, $token);
        $card = $this->getMatchingCard($customer, $card);
        if ($card === null) {
            $card = $this->stripe->createCardForCustomer($customer, $token);
        }

        $capture = $featureId == 0;
        
        // Facturer l'utilisateur (crée Charge de l'utilisateur)
        if ($capture) {
            //create the payment token
            $customerToken = $this->stripe->createToken(array(
                "customer" => $customer->id,
                "card" => $card->id,
                    ), array("stripe_account" => $stripeUserId));

            $charge = $this->stripe->createCharge(array(
                "amount" => $price + $gift,
                "currency" => "eur",
                "source" => $customerToken->id,
                "description" => "Purchase on redbricks.games {$name}",
                "application_fee" => $fee + $gift
                    ), array("stripe_account" => $stripeUserId));
            // Sauvegarder la transaction
            $this->purchaseTable->insert([
                'user_id' => $user->getId(),
                'feature_id' => $featureId,
                'game_id' => $gameId,
                'price' => $price,
                'country' => $card->country,
                'created_at' => date('Y-m-d H:i:s'),
                'stripe_id' => $charge->id
            ]);
        } else {
            //save the charge for later
            $this->pendingPurchaseTable->insert([
                'user_id' => $user->getId(),
                'feature_id' => $featureId,
                'game_id' => $gameId,
                'card_id' => $card->id,
                'price' => $price,
                'fee' => $fee,
                'created_at' => date('Y-m-d H:i:s')
            ]);

            if ($feeUser > 0) {
                $charge = $this->stripe->createCharge(array(
                    'amount' => $gift,
                    'currency' => 'eur',
                    'customer' => $customer->id,
                    'description' => 'donation to RedBricks',
                ));
                $this->purchaseTable->insert([
                    'user_id' => $user->getId(),
                    'feature_id' => 0,
                    'game_id' => 107,
                    'price' => $gift,
                    'country' => $card->country,
                    'created_at' => date('Y-m-d H:i:s'),
                    'stripe_id' => $charge->id
                ]);
            }
        }
    }

    public function featureFunding(int $featureId) {

        $purchase = $this->pendingPurchaseTable->findLastBy("feature_id", $featureId);
        if ($purchase != false) {
            //set_time_limit(30);
            $customer = $this->stripe->getCustomer($purchase->getCustomerId());
            $card = $customer->sources->retrieve($purchase->getCardId());

            //create the payment token
            $customerToken = $this->stripe->createToken(array(
                "customer" => $purchase->getCustomerId(),
                "card" => $purchase->getCardId(),
                    ), array("stripe_account" => $purchase->getStripeUserId()));

            //create the charge
            $charge = $this->stripe->createCharge(array(
                "amount" => $purchase->getPrice(),
                "currency" => "eur",
                "source" => $customerToken->id,
                "description" => "Purchase on redbricks.games {$purchase->projectName}",
                "application_fee" => $purchase->getFee()
                    ), array("stripe_account" => $purchase->getStripeUserId()));

            //save the transaction
            $this->purchaseTable->insert([
                'user_id' => $purchase->getUserId(),
                'feature_id' => $featureId,
                'game_id' => $purchase->getGameId(),
                'price' => $purchase->getPrice(),
                'country' => $card->country,
                'created_at' => date('Y-m-d H:i:s'),
                'stripe_id' => $charge->id
            ]);

            //delete the pending charge
            $this->pendingPurchaseTable->delete($purchase->getId());
            return true;
        }
        return false;
    }

    /**
     * @param Customer $customer
     * @param Card $card
     * @return null|Card
     */
    private function getMatchingCard(Customer $customer, Card $card): ?Card {
        foreach ($customer->sources->data as $datum) {
            if ($datum->fingerprint === $card->fingerprint) {
                return $datum;
            }
        }
        return null;
    }

    /**
     * Génère le client à partir de l'utilisateur
     * @param User $user
     * @param $token
     * @return Customer
     */
    private function findCustomerForUser(User $user, $token): Customer {
        $customerId = $this->stripeUserTable->findCustomerForUser($user);
        if ($customerId) {
            $customer = $this->stripe->getCustomer($customerId);
        } else {
            $customer = $this->stripe->createCustomer([
                'email' => $user->getEmail(),
                'source' => $token
            ]);
            $this->stripeUserTable->insert([
                'user_id' => $user->getId(),
                'customer_id' => $customer->id,
                'created_at' => date('Y-m-d H:i:s')
            ]);
        }
        return $customer;
    }

    private function findPlan(int $amount, string $stripeUserId): Plan {
        $plans = $this->stripe->allPlan([], ["stripe_account" => $stripeUserId])->data;
        $plan = null;
        foreach ($plans as $planResearch) {
            if ($planResearch->amount == $amount) {
                $plan = $planResearch;
            }
        }
        if ($plan == null) {
            $plan = $this->stripe->createPlan([
                "amount" => $amount,
                "interval" => "month",
                "product" => [
                    "name" => 'Monthly membership base fee',
                    "type" => "service"
                ],
                "currency" => "eur"
            ], array("stripe_account" => $stripeUserId));
        }
        return $plan;
    }

    public function subscription(int $amount, User $user, string $token, int $gameId) {
        $game = $this->gameTable->find($gameId);
        $this->gameTable->update($gameId, [
            "updated_at" => date("Y-m-d H:i:s")
        ]);
        $stripeUserId = $this->userTable->find($game->getUserId())->getStripeUserId();

        $customer = $this->findCustomerForUser($user, $token);

        $card = $this->getMatchingCard($customer, $this->stripe->getCardFromToken($token));

        $customerToken = $this->stripe->createToken(array(
            "customer" => $customer->id,
            "card" => $card->id,
                ), array("stripe_account" => $stripeUserId));

        /*echo '<pre>';
        var_dump($this->stripe->getToken($customerToken->id), $stripeUserId, $customerToken);
        echo '</pre>';
        die();*/

        $customerConnected = $this->stripe->createCustomer([
            "description" => 'Shared Customer',
            "source" => $customerToken->id,
            "email" => $user->getEmail()
                ], array("stripe_account" => $stripeUserId));

        $plan = $this->findPlan($amount, $stripeUserId);
        //echo '<pre>'; var_dump($plan, $customer); echo '</pre>'; die();
        $timestamp = strtotime("today first day of next month");
        $subscription = $this->stripe->createSubscription([
            "customer" => $customerConnected->id,
            "billing_cycle_anchor" => $timestamp,
            "items" => [
                [
                    "plan" => $plan->id
                ]
            ]
        ], array("stripe_account" => $stripeUserId));
        $this->subscriptionTable->insert([
            "user_id" => $user->getId(),
            "game_id" => $game->getId(),
            "subscription_id" => $subscription['id'],
            "plan_id" => $plan->id,
            "stripe_account" => $stripeUserId,
            "created_at" => date('Y-m-d H:i:s')
        ]);
    }

}
