<?php

namespace App\Shop;

use App\Shop\Action\TipsAction;
use App\Shop\Action\PurchaseListingAction;
use App\Shop\Action\PurchaseProcessAction;
use App\Shop\Action\PurchaseRecapAction;
use Framework\Auth\LoggedInMiddleware;
use Framework\Module;
use Framework\Renderer\RendererInterface;
use Framework\Router;
use Psr\Container\ContainerInterface;
use App\Shop\Action\StripeAccountAction;
use App\Shop\Action\CollectAction;
use App\Shop\Action\ContributorsListAction;
use App\Shop\Action\SubscriptionAction;
use App\Shop\Action\SubscriptionListingAction;
use App\Shop\Action\SubscriptionCrudAction;
use App\Shop\Action\RewardCrudAction;
use App\Shop\Action\RewardIndexAction;
use App\Shop\Action\PaymentAction;
use App\Shop\Action\RewardContributorsAction;
use App\Shop\Action\SubscriptionContributorsAction;

class ShopModule extends Module {

    const MIGRATIONS = __DIR__ . '/db/migrations';
    const SEEDS = __DIR__ . '/db/seeds';
    const DEFINITIONS = __DIR__ . '/definitions.php';

    public function __construct(ContainerInterface $container) {
        $container->get(RendererInterface::class)->addPath('shop', __DIR__ . '/views');
        $router = $container->get(Router::class);
        $router->get('/contributions/subscription/{gameId:[0-9]+}/{rewardId:[0-9]+}', [LoggedInMiddleware::class, SubscriptionAction::class], 'shop.subscription');
        $router->get('/contributions/{gameId:[0-9]+}/{featureId:[0-9]+}', TipsAction::class, 'shop');
        $router->post(
                '/contributions/{gameId}/{featureId}/recap', [LoggedInMiddleware::class, PurchaseRecapAction::class], 'shop.purchase'
        );
        $router->post(
                '/contributions/{gameId:[0-9]+}/{rewardId:[0-9]+}/process', [LoggedInMiddleware::class, PurchaseProcessAction::class], 'shop.process.subscription'
        );
        $router->post('/payment/{gameId:[0-9]+}/{rewardId:[0-9]+}', [LoggedInMiddleware::class, PaymentAction::class], 'shop.process.payment');
        $router->post(
                '/contributions/{gameId}/{featureId}/process', [LoggedInMiddleware::class, PurchaseProcessAction::class], 'shop.process'
        );
        $router->get(
                '/my-contributions/donations', [LoggedInMiddleware::class, PurchaseListingAction::class], 'shop.purchases.donations'
        );
        $router->get('/my-contributions/subscriptions', [LoggedInMiddleware::class, SubscriptionListingAction::class], 'shop.purchases.subscriptions');
        $router->crud('/my-contributions/subscriptions/manage', [LoggedInMiddleware::class, SubscriptionCrudAction::class], 'shop.purchases.subscriptions.manage');
        $router->get('/stripe-account/auth', [LoggedInMiddleware::class, StripeAccountAction::class], 'shop.stripe.account');

        $router->post('/contributions/collect/{id:[0-9]+}', [LoggedInMiddleware::class, CollectAction::class], 'shop.collect');
        $router->get('/home/{slug:[a-z\-0-9]+}-{id:[0-9]+}/contributors', ContributorsListAction::class, 'shop.contributors');
        $router->get('/rewards/index/{id:[0-9]+}', [LoggedInMiddleware::class, RewardIndexAction::class], 'shop.rewards');
        $router->crud('/rewards/{projectId:[0-9]+}', [LoggedInMiddleware::class, RewardCrudAction::class], 'shop.rewards');
        $router->get('/rewards/contributors/{projectId:[0-9]+}/{rewardId:[0-9]+}', RewardContributorsAction::class, 'shop.rewards.contributors');
        $router->get('/rewards/contributors/{projectId:[0-9]+}', SubscriptionContributorsAction::class, 'shop.rewards.allContributors');
    }

}
