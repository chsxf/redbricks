<?php

namespace App\Shop\Action;

use App\Blog\Table\GameTable;
use App\Blog\Table\FeatureTable;
use App\Shop\Table\PurchaseTable;
use App\Shop\Table\PendingPurchaseTable;
use Framework\Auth;
use Framework\Renderer\RendererInterface;

class PurchaseListingAction {

    /**
     * @var RendererInterface
     */
    private $renderer;

    /**
     * @var PurchaseTable
     */
    private $purchaseTable;
    
    /**
     *
     * @var PendingPurchaseTable
     */
    protected $pendingPurchaseTable;

    /**
     * @var Auth
     */
    private $auth;

    public function __construct(
    RendererInterface $renderer, GameTable $gameTable, FeatureTable $featureTable, PurchaseTable $purchaseTable, PendingPurchaseTable $pendingPurchaseTable, Auth $auth
    ) {
        $this->renderer = $renderer;
        $this->gameTable = $gameTable;
        $this->featureTable = $featureTable;
        $this->purchaseTable = $purchaseTable;
        $this->pendingPurchaseTable = $pendingPurchaseTable;
        $this->auth = $auth;
    }

    public function __invoke() {

        if ("admin" != $this->auth->getUser()->getRole()) {
            $purchases = $this->purchaseTable->findForUser($this->auth->getUser());
            $pendingPurchases = $this->pendingPurchaseTable->findForUser($this->auth->getUser());
        } else {
            $purchases = $this->purchaseTable->findAll();
        }
        $games = $this->gameTable->findAll();
        $features = $this->featureTable->findAll();
        return $this->renderer->render('@shop/purchases', compact('purchases', 'games', 'features', 'pendingPurchases'));
    }

}
