<?php

namespace App\Shop\Action;

use Framework\Renderer\RendererInterface;
use Framework\Auth;
use Psr\Http\Message\ServerRequestInterface;
use App\Auth\UserTable;
use Framework\Actions\RouterAwareAction;
use Framework\Router;
use Framework\Session\FlashService;

class StripeAccountAction {

    /**
     * @var RendererInterface
     */
    protected $renderer;

    /**
     * @var Auth
     */
    protected $auth;
    
    /**
     *
     * @var UserTable
     */
    protected $userTable;

    /**
     * @var string
     */
    private $stripeKey;
    
    /**
     * @var Router
     */
    protected $router;
    
    /**
     * @var FlashService
     */
    protected $flashService;
    
    use RouterAwareAction;
    
    const TOKEN_URI = 'https://connect.stripe.com/oauth/token';

    public function __construct(
    RendererInterface $renderer, Auth $auth, UserTable $userTable, string $stripeKey, Router $router, FlashService $flashService
    ) {
        $this->renderer = $renderer;
        $this->auth = $auth;
        $this->userTable = $userTable;
        $this->stripeKey = $stripeKey;
        $this->router = $router;
        $this->flashService = $flashService;
    }

    public function __invoke(ServerRequestInterface $request) {
        $params = $request->getQueryParams();
        if (!empty($params['code'])) {
            $token_request_body = array(
                'client_secret' => $this->stripeKey,
                'grant_type' => 'authorization_code',
                'client_id' => 'ca_DFkDDBPJo2eS4bhZXPMnUYhPeRRllDTO',
                'code' => $params['code']
            );

            $req = curl_init(self::TOKEN_URI);
            curl_setopt($req, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($req, CURLOPT_POST, true);
            curl_setopt($req, CURLOPT_POSTFIELDS, http_build_query($token_request_body));
            
            // TODO: Additional error handling
            $respCode = curl_getinfo($req, CURLINFO_HTTP_CODE);
            $resp = json_decode(curl_exec($req), true);
            curl_close($req);
            
            if (!empty($resp['stripe_user_id'])) {
                $user= $this->auth->getUser();
                $this->userTable->update($user->getId(), ['stripe_user_id' => $resp['stripe_user_id']]);
                $this->flashService->success(_('Your Stripe account is linked'));
            } else if (!empty($resp['error_description'])){
                $error = $resp['error_description'];
                $this->flashService->error(_('Error: ') . $error);
            }
        } else if (!empty($params['error_description'])) {
            $error = $params['error_description'];
            $this->flashService->error(_('Error: ') . $error);
        }

        return $this->redirect('blog.user.index');
    }

}
