<?php

namespace App\Shop\Action;

use App\Blog\Table\GameTable;
use App\Blog\Table\FeatureTable;
use App\Shop\Table\PurchaseTable;
use Framework\Renderer\RendererInterface;
use Psr\Http\Message\ServerRequestInterface;
use Framework\Actions\RouterAwareAction;
use App\Shop\PurchaseProduct;

class CollectAction {

    /**
     * @var RendererInterface
     */
    protected $renderer;

    /**
     * @var PurchaseTable
     */
    protected $purchaseTable;

    /**
     *
     * @var PurchaseProduct
     */
    protected $purchaseProduct;

    use RouterAwareAction;

    public function __construct(
    RendererInterface $renderer, GameTable $gameTable, FeatureTable $featureTable, PurchaseTable $purchaseTable, PurchaseProduct $purchaseProduct
    ) {
        $this->renderer = $renderer;
        $this->gameTable = $gameTable;
        $this->featureTable = $featureTable;
        $this->purchaseTable = $purchaseTable;
        $this->purchaseProduct = $purchaseProduct;
    }

    public function __invoke(ServerRequestInterface $request) {
        $featureId = $request->getAttribute("id");
        $feature = $this->featureTable->find($featureId);
        $game = $this->gameTable->find($feature->getProjectId());
        $this->gameTable->update($game->getId(), [
            "updated_at" => date("Y-m-d H:i:s")
        ]);

        $params = $request->getParsedBody();
        if (empty($params['confirm_collect'])) {
            $result = $this->purchaseProduct->featureFunding($featureId);
        } else {
            $result = true;
        }
        
        $estimatedDate = $params['estimatedDate'];
        $purchases = $this->purchaseTable->findForFeature($featureId);

        return $this->renderer->render('@shop/collect', compact('purchases', 'game', 'feature', 'result', 'estimatedDate'));
    }

}
