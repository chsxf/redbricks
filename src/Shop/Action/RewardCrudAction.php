<?php

namespace App\Shop\Action;

use Framework\Actions\CrudAction;
use Framework\Renderer\RendererInterface;
use Framework\Router;
use App\Shop\Table\RewardTable;
use Framework\Session\FlashService;
use Psr\Http\Message\ServerRequestInterface;
use Framework\Api\Stripe;
use App\Shop\Entity\Reward;
use App\Blog\Table\GameTable;
use App\Auth\UserTable;
use Framework\Session\SessionInterface;
use Framework\Auth;
use Framework\Database\Hydrator;

class RewardCrudAction extends CrudAction {

    /**
     *
     * @var RendererInterface
     */
    protected $renderer;

    /**
     *
     * @var Router
     */
    protected $router;

    /**
     *
     * @var RewardTable
     */
    protected $table;

    /**
     *
     * @var GameTable
     */
    protected $gameTable;

    /**
     *
     * @var UserTable
     */
    protected $userTable;

    /**
     *
     * @var FlashService
     */
    protected $flash;

    /**
     *
     * @var Stripe
     */
    protected $stripe;

    /**
     *
     * @var SessionInterface
     */
    protected $session;
    protected $acceptedParams = ["title", "project_id", "description", "amount", "contributors_max", "product_id", "plan_id"];

    public function __construct(
            RendererInterface $renderer, Router $router, RewardTable $table, FlashService $flash, GameTable $gameTable, UserTable $userTable, Stripe $stripe, SessionInterface $session
    ) {
        parent::__construct($renderer, $router, $table, $flash);
        $this->stripe = $stripe;
        $this->gameTable = $gameTable;
        $this->userTable = $userTable;
        $this->session = $session;
        $this->messages = [
            "create" => _("A reward has been created."),
            "edit" => _("The reward has been modified.")
        ];
    }

    public function create(ServerRequestInterface $request) {
        $item = new Reward();
        $item->setProjectId($request->getAttribute('projectId'));
        $game = $this->gameTable->find($request->getAttribute('projectId'));
        $this->validateAccessRights($game->getUserId(), $game->getId());
        if ($request->getMethod() === "POST") {
            $validator = $this->getValidator($request);
            if ($validator->isValid()) {
                $reward = $this->prePersist($request, $item);
                $this->table->insert($reward);
                $this->postPersist($request, $item);
                $this->flash->success($this->messages["create"]);

                return $this->redirect("shop.rewards", [
                            "id" => $item->getProjectId()
                ]);
            }
            Hydrator::hydrate($request->getParsedBody(), $item);
            $errors = $validator->getErrors();
        }

        return $this->renderer->render('@shop/rewards/create', compact("item", "errors"));
    }

    public function edit(ServerRequestInterface $request) {
        $item = $this->table->find($request->getAttribute("id"));
        $game = $this->gameTable->find($request->getAttribute('projectId'));
        $this->validateAccessRights($game->getUserId(), $game->getId());
        if ($request->getMethod() === "POST") {
            $validator = $this->getValidator($request);
            if ($validator->isValid()) {
                $reward = $this->prePersist($request, $item);
                array_pop($reward); //delete contributors_nb
                $this->table->update($item->getId(), $reward);
                $this->flash->success($this->messages["edit"]);
                $this->postPersist($request, $item);

                return $this->redirect("shop.rewards", [
                            "id" => $game->getId()
                ]);
            }
            $errors = $validator->getErrors();
            Hydrator::hydrate($request->getParsedBody(), $item);
        }
        return $this->renderer->render('@shop/rewards/edit', compact("item", "errors"));
    }

    protected function getValidator(ServerRequestInterface $request) {
        $validator = parent::getValidator($request)
                ->required("title", "amount", "contributors_max")
                ->length("title", 2, 200)
                ->length("description", 0, 500)
                ->numeric("amount")
                ->number("amount", 1)
                ->numeric("contributors_max");

        return $validator;
    }

    protected function prePersist(ServerRequestInterface $request, $reward): array {
        $params = $request->getParsedBody();

        if ($this->session->get('auth.role')) {
            $userId = $this->session->get('auth.user');
        } else {
            $userId = null;
        }

        if (!empty($params["projectId"])) {
            $params["project_id"] = $params["projectId"];
        }
        if ((!empty($params["unlimited"]) && $params["unlimited"] = "unlimited") || $params['contributors_max'] == 0) {
            $params["contributors_max"] = null;
        }

        $user = $this->userTable->find($userId);

        if ($reward->getProductId() == null) {
            $product = $this->stripe->createProduct([
                "name" => $params['title'],
                "type" => "service"
                    ], array("stripe_account" => $user->getStripeUserId()));
            $plan = $this->stripe->createPlan([
                "amount" => $params['amount'] * 100,
                "interval" => "month",
                "product" => $product->id,
                "currency" => "eur"
                    ], array("stripe_account" => $user->getStripeUserId()));
            $params['product_id'] = $product->id;
            $params['plan_id'] = $plan->id;
        } else {
            //$product = $this->stripe->getProduct($reward->getProductId(), array("stripe_account" => $user));
            $this->stripe->updateProduct($reward->getProductId(),
                    array("name" => $params['title']),
                    array("stripe_account" => $user->getStripeUserId()));
            $plan = $this->stripe->getPlan($reward->getPlanId(), ["stripe_account" => $user->getStripeUserId()]);
            if ($plan->amount != $params['amount'] * 100) {
                $plan->delete();
                $plan = $this->stripe->createPlan([
                    "amount" => $params['amount'] * 100,
                    "interval" => "month",
                    "product" => $reward->getProductId(),
                    "currency" => "eur"
                        ], array("stripe_account" => $user->getStripeUserId()));
                $params['plan_id'] = $plan->id;
            }
        }

        $params = array_filter($params, function ($key) {
            return in_array($key, $this->acceptedParams);
        }, ARRAY_FILTER_USE_KEY);
        return array_merge($params, ["contributors_nb" => 0]);
    }

    protected function validateAccessRights(int $itemUserId, int $gameId) {
        $role = $this->session->get('auth.role');
        $userId = $this->session->get('auth.user');
        if ($role == null || ($role == 'user' && $userId != $itemUserId && !$this->contributorTable->isContributor($gameId, $userId))) {
            throw new Auth\ForbiddenException();
        }
    }

}
