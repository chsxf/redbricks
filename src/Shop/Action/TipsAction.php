<?php

namespace App\Shop\Action;

use App\Shop\Table\PurchaseTable;
use App\Blog\Table\GameTable;
use App\Blog\Table\FeatureTable;
use Framework\Renderer\RendererInterface;
use Psr\Http\Message\ServerRequestInterface;

class TipsAction {

    /**
     * @var RendererInterface
     */
    private $renderer;

    /**
     * @var GameTable
     */
    private $gameTable;

    /**
     * @var string
     */
    private $stripeKey;

    public function __construct(
    RendererInterface $renderer, GameTable $gameTable, FeatureTable $featureTable, PurchaseTable $purchaseTable, string $stripeKey
    ) {
        $this->renderer = $renderer;
        $this->gameTable = $gameTable;
        $this->featureTable = $featureTable;
        $this->purchaseTable = $purchaseTable;
        $this->stripeKey = $stripeKey;
    }

    public function __invoke(ServerRequestInterface $request) {

        $params['gameId'] = $request->getAttribute('gameId');
        $game = $this->gameTable->find($params['gameId']);
        $params['featureId'] = $request->getAttribute('featureId');
        if ($params['featureId'] != 0) {
            $feature = $this->featureTable->find($params['featureId']);
        }
        $stripeKey = $this->stripeKey;

        return $this->renderer->render('@shop/index', compact('game', 'feature', 'stripeKey'));
    }

}
