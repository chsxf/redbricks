<?php
namespace App\User;

use Framework\Module;
use Framework\Renderer\RendererInterface;
use Framework\Renderer\TwigRenderer;
use Framework\Router;

class UserModule extends Module
{
   
    const DEFINITIONS= __DIR__ . "/config.php";
    
    public function __construct(
        RendererInterface $renderer,
        Router $router,
        UserTwigExtension $userTwigExtension,
        string $prefix
    ) {
         
        $renderer->addPath("user", __DIR__ . "/views");
        $router->get($prefix, UserDashboardAction::class, "user");
        if ($renderer instanceof TwigRenderer) {
            $renderer->getTwig()->addExtension($userTwigExtension);
        }
    }
}
