<?php

namespace App\Account\Action;

use App\Auth\UserTable;
use Framework\Actions\RouterAwareAction;
use Framework\Renderer\RendererInterface;
use Framework\Router;
use Framework\Session\FlashService;
use Framework\Validator;
use Psr\Http\Message\ServerRequestInterface;

class SignupAction {

    /**
     * @var RendererInterface
     */
    private $renderer;

    /**
     * @var UserTable
     */
    private $userTable;

    /**
     * @var Router
     */
    private $router;

    /**
     * @var FlashService
     */
    private $flashService;

    /**
     * @var \Swift_Mailer
     */
    private $mailer;

    /**
     * @var string
     */
    private $from;

    use RouterAwareAction;

    public function __construct(
    RendererInterface $renderer, UserTable $userTable, Router $router, FlashService $flashService, string $from, \Swift_Mailer $mailer
    ) {
        $this->renderer = $renderer;
        $this->userTable = $userTable;
        $this->router = $router;
        $this->flashService = $flashService;
        $this->from = $from;
        $this->mailer = $mailer;
    }

    public function __invoke(ServerRequestInterface $request) {
        if ($request->getMethod() === 'GET') {
            return $this->renderer->render('@account/signup');
        }
        $params = $request->getParsedBody();
        $params['activation_key'] = md5(microtime(TRUE) * 100000);
        $validator = (new Validator($params))
                ->required('username', 'email', 'password', 'password_confirm')
                ->length('username', 5)
                ->email('email')
                ->specialCharacters('username')
                ->confirm('password')
                ->length('password', 3)
                ->unique('username', $this->userTable)
                ->unique('email', $this->userTable);
        if ($validator->isValid()) {
            $userParams = [
                'username' => $params['username'],
                'email' => $params['email'],
                'password' => password_hash($params['password'], PASSWORD_DEFAULT),
                'displayname' => $params['username'],
                'role' => 'user',
                'active' => 0,
                'activation_key' => $params['activation_key'],
                'created_at' => (new \DateTime())->format("Y-m-d H:i:s")
            ];
            $this->userTable->insert($userParams);
            $text = _("An email has been sent to you. You must validate your account to create it.");
            $this->flashService->success($text);
            $message = new \Swift_Message('Your registration on RedBricks');
            $message->setBody($this->renderer->render('@account/email/signup.text', $params));
            $message->addPart($this->renderer->render('@account/email/signup.html', $params), 'text/html');
            $message->setTo($params['email']);
            $message->setFrom($this->from);
            $this->mailer->send($message);
            return $this->redirect('blog.index');
        }
        $errors = $validator->getErrors();
        return $this->renderer->render('@account/signup', [
                    'errors' => $errors,
                    'user' => [
                        'username' => $params['username'],
                        'email' => $params['email']
                    ]
        ]);
    }

}
