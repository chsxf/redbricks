<?php

namespace App\Badge\Entity;

class Badge {

    /**
     *
     * @var string
     */
    protected $name;

    /**
     *
     * @var string
     */
    protected $description;

    /**
     * 
     * @return string
     */
    function getName(): string {
        return $this->name;
    }

    /**
     * 
     * @return string
     */
    function getDescription(): string {
        return $this->description;
    }

    /**
     * 
     * @param string $name
     */
    function setName(string $name) {
        $this->name = $name;
    }

    /**
     * 
     * @param string $description
     */
    function setDescription(string $description) {
        $this->description = $description;
    }

}
