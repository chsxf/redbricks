<?php

namespace App\Badge\Entity;

use App\Badge\Entity\Badge;

class BadgeUsers extends Badge {

    /**
     *
     * @var int
     */
    protected $id;

    /**
     *
     * @var int
     */
    protected $badgeId;

    /**
     *
     * @var int
     */
    protected $userId;

    /**
     * 
     * @return int
     */
    function getId(): int {
        return $this->id;
    }

    /**
     * 
     * @return int
     */
    function getUserId(): int {
        return $this->userId;
    }

    /**
     * 
     * @param int $id
     */
    function setId(int $id) {
        $this->id = $id;
    }

    /**
     * 
     * @param int $userId
     */
    function setUserId(int $userId) {
        $this->userId = $userId;
    }

    /**
     * 
     * @return int
     */
    function getBadgeId(): int {
        return $this->badgeId;
    }

    /**
     * 
     * @param int $badgeId
     */
    function setBadgeId(int $badgeId) {
        $this->badgeId = $badgeId;
    }

}
