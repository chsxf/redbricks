<?php

namespace App\Badge\Table;

use Framework\Database\Table;
use Framework\Database\Query;
use App\Badge\Entity\Badge;

class BadgeTable extends Table {

    protected $table = "badge";
    protected $entity = Badge::class;

    public function findAll(): Query {
        $name = 'b.name_' . getenv("LANG");
        $description = 'b.description_' . getenv("LANG");
        return $this->makeQuery()
                        ->select("b.id, $name as name, $description as description");
    }

}
