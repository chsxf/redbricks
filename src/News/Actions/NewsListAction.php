<?php

namespace App\News\Actions;

use Framework\Renderer\RendererInterface;
use Framework\Response\RedirectResponse;
use Psr\Http\Message\ServerRequestInterface;
use Framework\Router;
use App\Blog\Table\GameTable;
use App\News\Table\NewsTable;
use App\Bug\Table\BugTable;
use App\Comments\Table\CommentsTable;
use App\Blog\Table\FeatureTable;
use App\Survey\Table\SurveyTable;

class NewsListAction {

    /**
     * @var RendererInterface
     */
    private $renderer;

    public function __construct(
            RendererInterface $renderer, Router $router, GameTable $gameTable, NewsTable $newsTable, FeatureTable $featureTable, CommentsTable $commentsTable, BugTable $bugTable, SurveyTable $surveyTable
    ) {
        $this->renderer = $renderer;
        $this->router = $router;
        $this->gameTable = $gameTable;
        $this->newsTable = $newsTable;
        $this->bugTable = $bugTable;
        $this->commentsTable = $commentsTable;
        $this->featureTable = $featureTable;
        $this->surveyTable = $surveyTable;
    }

    /**
     * @param ServerRequestInterface $request
     * @return RedirectResponse|string
     */
    public function __invoke(ServerRequestInterface $request) {
        $slug = $request->getAttribute("slug");
        $gameId = $request->getAttribute("id");
        $game = $this->gameTable->findShow($gameId);
        $news = $this->newsTable->findShow($gameId);

        $nbFeatures = $this->featureTable->findShow($gameId)->where("s.id>1")->count("*");
        $nbBugs = $this->bugTable->findShow($gameId)->count("*");
        $nbComments = $this->commentsTable->findCount($gameId);
        $nbNews = $news->count("*");
        $nbSurveys = $this->surveyTable->findShow($gameId)->count("*");

        if ($game->getSlug() !== $slug) {
            return $this->redirect("blog.show", [
                        "slug" => $game->getSlug(),
                        "id" => $game->getId()
            ]);
        }

        return $this->renderer->render('@news/list', [
                    "game" => $game,
                    "news" => $news,
                    "nbFeatures" => $nbFeatures,
                    "nbBugs" => $nbBugs,
                    "nbComments" => $nbComments,
                    "nbNews" => $nbNews,
                    "nbSurveys" => $nbSurveys
        ]);
    }

}
