<?php

namespace App\Notification\Table;

use Framework\Database\Table;
use App\Shop\Table\PendingPurchaseTable;
use App\Shop\Table\PurchaseTable;
use App\Auth\UserTable;
use App\Blog\Table\FavoriteTable;
use Framework\Database\Query;

class NotificationTable extends Table {

    protected $table = "notifications";

    /**
     * 
     * @param int $userId
     * @param array $params
     * @return bool
     */
    public function update(int $userId, array $params): bool {
        $fieldQuery = $this->buildFieldQuery($params);
        $params["user_id"] = $userId;
        $query = $this->pdo->prepare("UPDATE {$this->table} SET $fieldQuery WHERE user_id= :user_id");
        return $query->execute($params);
    }

    /**
     * 
     * @param int $gameId
     * @param string $key
     * @return \PDOStatement
     */
    public function findUsers(int $gameId, string $key): \PDOStatement {
        $pendingPurchase = new PendingPurchaseTable($this->pdo);
        $purchase = new PurchaseTable($this->pdo);
        $favorite = new FavoriteTable($this->pdo);
        $user = new UserTable($this->pdo);
        $query = $this->pdo->prepare("SELECT u.email FROM {$this->table} as n, {$purchase->getTable()} as p, {$user->getTable()} as u WHERE u.id = n.user_id AND p.user_id = n.user_id AND $key = 1 AND p.game_id = $gameId"
                . " UNION SELECT u.email FROM {$this->table} as n, {$pendingPurchase->getTable()} as pp, {$user->getTable()} as u WHERE u.id = n.user_id AND pp.user_id = n.user_id AND $key = 1 AND pp.game_id = $gameId"
                . " UNION SELECT u.email FROM {$this->table} as n, {$favorite->getTable()} as f, {$user->getTable()} as u WHERE u.id = n.user_id AND f.user_id = n.user_id AND f.game_id = $gameId AND $key = 1");
        $query->execute();
        return $query;
    }

}
