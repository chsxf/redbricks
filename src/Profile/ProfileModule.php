<?php

namespace App\Profile;

use Framework\Module;
use Framework\Renderer\RendererInterface;
use Framework\Router;

class ProfileModule extends Module
{
    public function __construct(Router $router, RendererInterface $renderer)
    {
        $renderer->addPath('profile', __DIR__);
        $router->get('/profile/{id:[-0-9]+}', ProfileAction::class, 'profile');
    }
}