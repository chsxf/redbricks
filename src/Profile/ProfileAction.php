<?php

namespace App\Profile;

use App\Auth\UserTable;
use App\Blog\Table\FeatureTable;
use App\Blog\Table\GameTable;
use App\Badge\Table\BadgeUsersTable;
use Framework\Actions\RouterAwareAction;
use Framework\Database\NoRecordException;
use Framework\Renderer\RendererInterface;
use Framework\Response\RedirectResponse;
use Framework\Router;
use Framework\Session\FlashService;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Expressive\Router\RouterInterface;
use App\Blog\Table\FavoriteTable;
use App\Shop\Table\SubscriptionTable;
use Framework\Auth;

class ProfileAction {

    /**
     * @var RendererInterface
     */
    private $renderer;

    /**
     * @var UserTable
     */
    private $userTable;

    /**
     * @var FlashService
     */
    private $flashService;

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @var GameTable
     */
    private $gameTable;

    /**
     * @var FeatureTable
     */
    private $featureTable;

    /**
     *
     * @var BadgeUsersTable
     */
    protected $badgeUsersTable;

    /**
     *
     * @var FavoriteTable
     */
    protected $favoriteTable;

    /**
     *
     * @var SubscriptionTable
     */
    protected $subscriptionTable;

    /**
     *
     * @var Auth
     */
    protected $auth;
    protected $routePrefix = "blog.index";

    use RouterAwareAction;

    public function __construct(
            RendererInterface $renderer, UserTable $userTable, GameTable $gameTable, FeatureTable $featureTable, BadgeUsersTable $badgeUsersTable, FlashService $flashService, Router $router, FavoriteTable $favoriteTable, SubscriptionTable $subscriptionTable, Auth $auth
    ) {
        $this->renderer = $renderer;
        $this->userTable = $userTable;
        $this->gameTable = $gameTable;
        $this->featureTable = $featureTable;
        $this->badgeUsersTable = $badgeUsersTable;
        $this->flashService = $flashService;
        $this->router = $router;
        $this->favoriteTable = $favoriteTable;
        $this->subscriptionTable = $subscriptionTable;
        $this->auth = $auth;
    }

    /**
     * @param ServerRequestInterface $request
     * @return RedirectResponse|string
     */
    public function __invoke(ServerRequestInterface $request) {
        $this->renderer->addGlobal("routePrefix", $this->routePrefix);
        $userId = $request->getAttribute('id');
        if ($userId === null) {
            return $this->redirect('blog.index');
        }
        try {
            $user = $this->userTable->findBy('id', $userId);

            $supportedGames = $this->gameTable->findAllFinanced($user->getId());
            $contributorsNb = $this->subscriptionTable->findAllContributors();
            $liens_platforms = $this->gameTable->findAllPlatform();
            $features = $this->featureTable->findAll();
            $badges = $this->badgeUsersTable->findForUser($user->getId());
            if ($this->auth->getUser() != null) {
                $currentUser = $this->auth->getUser();
                $favorites = $this->favoriteTable->findAllForUser($currentUser->getId());
            }
            $games = $this->gameTable->findAllUser($user->getId());
            return $this->renderer->render('@profile/profile', compact('user', 'games', 'liens_platforms', 'features', 'supportedGames', 'badges', 'favorites', 'money_raised', 'money_raised_pending', 'subscriptions', 'contributorsNb'));
        } catch (NoRecordException $exception) {
            $this->flashService->error(_("This account doesn't exist"));
        }
        return $this->redirect('blog.index');
    }

}
